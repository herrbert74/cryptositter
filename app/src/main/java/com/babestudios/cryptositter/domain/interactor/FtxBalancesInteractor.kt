package com.babestudios.cryptositter.domain.interactor

import com.babestudios.cryptositter.data.getNonce
import com.babestudios.cryptositter.data.model.ftx.FtxBalance
import com.babestudios.cryptositter.data.model.ftx.FtxBalancesResponse
import com.babestudios.cryptositter.data.model.ftx.FtxCandlesResponse
import com.babestudios.cryptositter.data.model.ftx.FtxTickerResponse
import com.babestudios.cryptositter.data.network.base.KEY_CACHE_AGE_LOWER_LIMIT
import com.babestudios.cryptositter.data.network.ftx.FtxCache
import com.babestudios.cryptositter.data.network.ftx.FtxRepository
import com.babestudios.cryptositter.domain.model.Balance
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.rxkotlin.Flowables
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.Executors
import javax.inject.Inject

class FtxBalancesInteractor @Inject constructor(
		private var ftxCache: FtxCache,
		private var ftxRepository: FtxRepository
) : ExchangeInteractor {

	private val excludedTickerSet = emptySet<String>()

	//region complex calls

	override fun getBalancesWithDetails(exchangeWithOwner: String): Flowable<Map<String, List<Balance>>> {
		return Flowables.combineLatest(
				getBalancesResponse(exchangeWithOwner).subscribeOn(Schedulers.newThread())
						.onErrorResumeNext(Flowable.just(FtxBalancesResponse(ArrayList(), false))),
				getTickerResponse("BTC/USD").subscribeOn(Schedulers.newThread())
		) { balances: FtxBalancesResponse, btcUsdLastPrice: FtxTickerResponse ->
			balances.result.forEach {
				it.lastBtcUsdPrice = btcUsdLastPrice.result.last
			}
			val result = balances.result
			result
		}
				.flatMapIterable { response -> response }
				.map { item: FtxBalance -> item.convertToBalance() }
				.filter { balance -> balance.amount > 0.0 }
				.toList()
				.map { t -> mapOf(exchangeWithOwner to t) }
				.toFlowable()
	}

	override fun getBalanceDetails(balances: List<Balance>?): Flowable<Balance> {
		return Flowable.fromArray(balances)
				.flatMapIterable { response -> response }
				.flatMap({ balance ->
					Flowables.combineLatest(
							getTickerResponse(ftxRepository.getBitcoinTickerSymbol(balance.symbol))
									.subscribeOn(Schedulers.newThread()),
							getMarketSummaryResponse(ftxRepository.getBitcoinTickerSymbol(balance.symbol))
									.subscribeOn(Schedulers.newThread())
					) { ticker, marketSummary -> addDetailsToBalance(balance, ticker, marketSummary) }
							.subscribeOn(Schedulers.from(Executors.newCachedThreadPool()))
							.onErrorResumeNext { _: Throwable ->
								balance.lastPrice = Double.NaN
								Flowable.just(balance)
							}
				}, { balance, _ -> balance })
	}

	//endregion

	//region atomic calls

	private fun getBalancesResponse(exchangeWithOwner: String): Flowable<FtxBalancesResponse> {
		return ftxCache.getBalancesResponse(exchangeWithOwner)
				.switchIfEmpty(getBalancesAndCacheIt(exchangeWithOwner))
				.toFlowable()
				.doOnNext {
					if (Date(it.updateTime).before(KEY_CACHE_AGE_LOWER_LIMIT)) {
						getBalancesAndCacheIt(exchangeWithOwner)
								.onErrorResumeNext(Single.just(FtxBalancesResponse(emptyList(), false)))
								.subscribe()
					}
				}
	}

	private fun getBalancesAndCacheIt(exchangeWithOwner: String): Single<FtxBalancesResponse> {
		return ftxRepository.getFtxBalancesResponse(exchangeWithOwner)
				.firstOrError()
				.doOnSuccess {
					ftxCache.putBalancesResponse(exchangeWithOwner, it)
				}
	}

	private fun getTickerResponse(symbol: String): Flowable<FtxTickerResponse> {
		return ftxCache.getTickerResponse(symbol)
				.switchIfEmpty(getTickerAndCacheIt(symbol))
				.toFlowable()
				.doOnNext {
					if (Date(it.updateTime).before(KEY_CACHE_AGE_LOWER_LIMIT)) {
						getTickerAndCacheIt(symbol).subscribe()
					}
				}
	}

	private fun getTickerAndCacheIt(symbol: String): Single<FtxTickerResponse> {
		return ftxRepository.getFtxTicker(symbol)
				.firstOrError()
				.doOnSuccess {
					ftxCache.putTickerResponse(it, symbol)
				}
	}

	private fun getMarketSummaryResponse(symbol: String): Flowable<FtxCandlesResponse> {
		return ftxCache.get24hResponse(symbol)
				.switchIfEmpty(getCandlesAndCacheIt(symbol))
				.toFlowable()
				.doOnNext {
					if (Date(it.updateTime).before(KEY_CACHE_AGE_LOWER_LIMIT)) {
						getCandlesAndCacheIt(symbol).subscribe()
					}
				}
	}

	private fun getCandlesAndCacheIt(
			symbol: String
	): Single<FtxCandlesResponse> {
		return ftxRepository.getFtxCandles(
				symbol,
				"3600",
				"24",
				getNonce().toString(),
				getNonce().toString()
		)
				.firstOrError()
				.doOnSuccess {
					ftxCache.put24hResponse(it, symbol)
				}
	}

	//endregion

	//region utility

	private fun addDetailsToBalance(balance: Balance, ticker: FtxTickerResponse, candles: FtxCandlesResponse): Balance {
		val close24hAgo = candles.result[0].close
		balance.lastPrice = ticker.result.last
		balance.dailyChange = ticker.result.last - close24hAgo
		balance.dailyChangePercentage = (ticker.result.last - close24hAgo) / close24hAgo * 100
		return balance
	}

	fun clearCache() {
		ftxCache.clearResponses()
	}

	private fun filterExclusionsFromBalances(balancesResponse: FtxBalancesResponse): FtxBalancesResponse {
		val list = balancesResponse.result
		balancesResponse.result = ArrayList(list.filter { ftxBalance -> !excludedTickerSet.contains(ftxBalance.coin) })
		return balancesResponse
	}

	//endregion

}
