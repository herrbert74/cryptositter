package com.babestudios.cryptositter.domain.interactor

import com.babestudios.cryptositter.data.model.bittrex.BittrexBalance
import com.babestudios.cryptositter.data.model.bittrex.BittrexBalancesResponse
import com.babestudios.cryptositter.data.model.bittrex.BittrexGetTickerResponse
import com.babestudios.cryptositter.data.model.bittrex.BittrexMarketSummaryResponse
import com.babestudios.cryptositter.data.network.base.KEY_CACHE_AGE_LOWER_LIMIT
import com.babestudios.cryptositter.data.network.bittrex.BittrexCache
import com.babestudios.cryptositter.data.network.bittrex.BittrexRepository
import com.babestudios.cryptositter.domain.model.Balance
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.rxkotlin.Flowables
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.Executors
import javax.inject.Inject


class BittrexBalancesInteractor @Inject constructor(
		private var bittrexCache: BittrexCache,
		private var bittrexRepository: BittrexRepository
) : ExchangeInteractor {

	private val excludedTickerSet = setOf("BTXCRD", "USD") //Bittrex Credit, doesn't have value

	//region complex calls

	override fun getBalancesWithDetails(exchangeWithOwner: String): Flowable<Map<String, List<Balance>>> {
		return Flowables.combineLatest(
				getBalancesResponse(exchangeWithOwner)
						.subscribeOn(Schedulers.newThread())
						.onErrorResumeNext(
								Flowable.just(BittrexBalancesResponse(ArrayList()))
						),
				getTickerResponse("BTC-USDT").subscribeOn(Schedulers.newThread())
		) { balancesResponse: BittrexBalancesResponse, btcUsdLastPrice: BittrexGetTickerResponse ->
			balancesResponse.balances.forEach { it.lastBtcUsdPrice = btcUsdLastPrice.lastTradeRate }
			balancesResponse.balances
		}
				.flatMapIterable { response -> response }
				.map { item: BittrexBalance -> item.convertToBalance() }
				.filter { balance -> balance.amount > 0.0 && !excludedTickerSet.contains(balance.symbol) }
				.toList()
				.map { t -> mapOf(exchangeWithOwner to t) }
				.toFlowable()
	}

	override fun getBalanceDetails(balances: List<Balance>?): Flowable<Balance> {
		return Flowable.fromArray(balances)
				.flatMapIterable { response -> response }
				.flatMap({ balance ->
					Flowables.combineLatest(
							getTickerResponse(bittrexRepository.getBitcoinTickerSymbol(balance.symbol))
									.subscribeOn(Schedulers.newThread()),
							getMarketSummaryResponse(bittrexRepository.getBitcoinTickerSymbol(balance.symbol))
									.subscribeOn(Schedulers.newThread())
					) { ticker, marketSummary -> addDetailsToBalance(balance, ticker, marketSummary) }
							.subscribeOn(Schedulers.from(Executors.newCachedThreadPool()))
							.onErrorResumeNext { _: Throwable ->
								balance.lastPrice = Double.NaN
								Flowable.just(balance)
							}
				}, { balance, _ -> balance })
	}

	//endregion

	//region atomic calls

	private fun getBalancesResponse(exchangeWithOwner: String): Flowable<BittrexBalancesResponse> {
		return bittrexCache.getBalancesResponse()
				.switchIfEmpty(getBalancesAndCacheIt(exchangeWithOwner))
				.toFlowable()
				.doOnNext {
					if (Date(it.updateTime).before(KEY_CACHE_AGE_LOWER_LIMIT)) {
						getBalancesAndCacheIt(exchangeWithOwner)
								.onErrorResumeNext(Single.just(BittrexBalancesResponse(arrayListOf())))
								.subscribe()
					}
				}
	}

	private fun getBalancesAndCacheIt(exchangeWithOwner: String): Single<BittrexBalancesResponse> {
		return bittrexRepository.getBittrexBalancesResponse(exchangeWithOwner)
				.firstOrError()
				.map { BittrexBalancesResponse(it) }
				.doOnSuccess {
					bittrexCache.putBalancesResponse(it)
				}
	}

	private fun getTickerResponse(symbol: String): Flowable<BittrexGetTickerResponse> {
		return bittrexCache.getTickerResponse(symbol)
				.switchIfEmpty(getTickerAndCacheIt(symbol))
				.toFlowable()
				.doOnNext {
					if (Date(it.updateTime).before(KEY_CACHE_AGE_LOWER_LIMIT)) {
						getTickerAndCacheIt(symbol).subscribe()
					}
				}
	}

	private fun getTickerAndCacheIt(symbol: String): Single<BittrexGetTickerResponse> {
		return bittrexRepository.getBittrexTicker(symbol)
				.firstOrError()
				.doOnSuccess {
					bittrexCache.putTickerResponse(it, symbol)
				}
	}

	private fun getMarketSummaryResponse(symbol: String): Flowable<BittrexMarketSummaryResponse> {
		return bittrexCache.get24hResponse(symbol)
				.switchIfEmpty(getMarketSummaryAndCacheIt(symbol))
				.toFlowable()
				.doOnNext {
					if (Date(it.updateTime).before(KEY_CACHE_AGE_LOWER_LIMIT)) {
						getMarketSummaryAndCacheIt(symbol).subscribe()
					}
				}
	}

	private fun getMarketSummaryAndCacheIt(symbol: String): Single<BittrexMarketSummaryResponse> {
		return bittrexRepository.getBittrexMarketSummary(symbol)
				.firstOrError()
				.doOnSuccess {
					bittrexCache.put24hResponse(it, symbol)
				}
	}
	//endregion

	//region utility

	private fun addDetailsToBalance(balance: Balance, ticker: BittrexGetTickerResponse, marketSummary: BittrexMarketSummaryResponse): Balance {
		balance.lastPrice = ticker.lastTradeRate
		val prevDayValue = ticker.lastTradeRate - (ticker.lastTradeRate * marketSummary.percentChange)
		balance.dailyChange = ticker.lastTradeRate - prevDayValue
		balance.dailyChangePercentage = marketSummary.percentChange
		return balance
	}

	fun clearCache() {
		bittrexCache.clearResponses()
	}

	private fun filterExclusionsFromBalances(balancesResponse: BittrexBalancesResponse): BittrexBalancesResponse {
		val list = balancesResponse.balances.toMutableList()
		return BittrexBalancesResponse(list.filter { bittrexBalance ->
			!excludedTickerSet.contains(bittrexBalance.currencySymbol)
		}.toList())
	}
	//endregion
}