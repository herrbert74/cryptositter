package com.babestudios.cryptositter.domain.interactor

import com.babestudios.cryptositter.domain.model.Balance
import com.babestudios.cryptositter.data.model.bitfinex.BitfinexBalance
import com.babestudios.cryptositter.data.model.bitfinex.BitfinexBalancesResponse
import com.babestudios.cryptositter.data.model.bitfinex.BitfinexTickerResponse
import com.babestudios.cryptositter.data.network.base.KEY_CACHE_AGE_LOWER_LIMIT
import com.babestudios.cryptositter.data.network.bitfinex.BitfinexCache
import com.babestudios.cryptositter.data.network.bitfinex.BitfinexRepository
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.rxkotlin.Flowables
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.Executors
import javax.inject.Inject
import kotlin.collections.ArrayList


class BitfinexBalancesInteractor @Inject constructor(
		private var bitfinexCache: BitfinexCache,
		private var bitfinexRepository: BitfinexRepository
) : ExchangeInteractor {

	private val excludedTickerSet = emptySet<String>()
	private lateinit var btcUsdTicker: ArrayList<Double>

	//region complex calls

	override fun getBalancesWithDetails(exchangeWithOwner: String): Flowable<Map<String, List<Balance>>> {
		return Flowables.combineLatest(
				getBalancesResponse(exchangeWithOwner)
						.onErrorResumeNext(Flowable.just(ArrayList())),
				getTickerResponse("tBTCUSD").subscribeOn(Schedulers.newThread())
		) { balances, btcUsdLastPrice ->
			btcUsdTicker = btcUsdLastPrice
			val bitfinexBalances = ArrayList<BitfinexBalance>()
			balances.forEach {
				val bitfinexBalance = createBalanceFromArrayList(it)
				bitfinexBalance.lastBtcUsdPrice = btcUsdLastPrice[2]
				bitfinexBalances.add(bitfinexBalance)
			}
			bitfinexBalances
		}
				.flatMapIterable { response -> response }
				.map { item -> item.convertToBalance() }
				.filter { balance -> balance.amount > 0.0 }
				.toList()
				.map { t -> mapOf(exchangeWithOwner to t) }
				.toFlowable()
	}

	override fun getBalanceDetails(balances: List<Balance>?): Flowable<Balance> {
		return Flowable.fromArray(balances)
				.flatMapIterable { response -> response }
				.flatMap({ balance ->
					val symbol = getBitcoinTickerSymbol(balance.symbol)
					if (symbol == "tBTCUSD") {
						Flowable.just(btcUsdTicker).subscribeOn(Schedulers.from(Executors.newCachedThreadPool()))
					} else {
						getTickerResponse(symbol)
								.subscribeOn(Schedulers.from(Executors.newCachedThreadPool()))
								.onErrorResumeNext { _: Throwable ->
									Flowable.just(arrayListOf(Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN))
								}

					}
				}, { balance, ticker -> addTickerToBalance(balance, ticker) })
	}

	//endregion

	//region atomic calls

	private fun getBalancesResponse(exchangeWithOwner: String): Flowable<ArrayList<ArrayList<Any>>> {
		return bitfinexCache.getBalancesResponse()
				.doOnSuccess {
					if (it != null && Date(it.updateTime).before(KEY_CACHE_AGE_LOWER_LIMIT)) {
						getBalancesAndCacheIt(exchangeWithOwner)
								.onErrorResumeNext(Single.just(arrayListOf()))
								.subscribe()
					}
				}
				.map { response -> response.balances }
				.switchIfEmpty(getBalancesAndCacheIt(exchangeWithOwner))
				.toFlowable()
	}

	private fun getBalancesAndCacheIt(exchangeWithOwner: String): Single<ArrayList<ArrayList<Any>>> {
		return bitfinexRepository.getBitfinexBalancesResponse(exchangeWithOwner)
				.firstOrError()
				.doOnSuccess {
					bitfinexCache.putBalancesResponse(BitfinexBalancesResponse(it))
				}
	}

	private fun getTickerResponse(symbol: String): Flowable<ArrayList<Double>> {
		return bitfinexCache.getTickerResponse(symbol)
				.doOnSuccess {
					if (Date(it.updateTime).before(KEY_CACHE_AGE_LOWER_LIMIT)) {
						getTickerAndCacheIt(symbol).subscribe()
					}
				}
				.map { response -> response.ticker }
				.switchIfEmpty(getTickerAndCacheIt(symbol))
				.toFlowable()

	}

	private fun getTickerAndCacheIt(symbol: String): Single<ArrayList<Double>> {
		return bitfinexRepository.getBitfinexTicker(symbol)
				.firstOrError()
				.doOnSuccess {
					bitfinexCache.putTickerResponse(BitfinexTickerResponse(it), symbol)
				}
	}

	//endregion

	//region utility

	private fun getBitcoinTickerSymbol(symbol: String): String {
		return when (symbol) {
			"BTC" -> "tBTCUSD"
			"USD" -> "tBTCUSD"
			else -> "t${symbol}BTC"
		}
	}

	private fun addTickerToBalance(balance: Balance, ticker: ArrayList<Double>): Balance {
		balance.dailyChange = ticker[4]
		balance.dailyChangePercentage = ticker[5]
		balance.lastPrice = ticker[6]
		return balance
	}

	private fun filterExclusionsFromBalances(balancesResponse: BitfinexBalancesResponse): BitfinexBalancesResponse {
		val list = balancesResponse.balances
		balancesResponse.balances = java.util.ArrayList(list.filter {
			val bitfinexBalance = createBalanceFromArrayList(it)
			!excludedTickerSet.contains(bitfinexBalance.currency)
		})
		return balancesResponse
	}

	@Suppress("SENSELESS_COMPARISON") //it[4] can be null!!
	private fun createBalanceFromArrayList(it: java.util.ArrayList<Any>) =
			BitfinexBalance(type = it[0].toString(), currency = it[1].toString(), amount = it[2].toString(), available = if (it[4] != null) it[4].toString() else "0.0")

	fun clearCache() {
		bitfinexCache.clearResponses()
	}

	//endregion
}