package com.babestudios.cryptositter.domain.interactor

import com.babestudios.cryptositter.domain.model.Balance
import io.reactivex.Flowable

interface ExchangeInteractor {
	fun getBalancesWithDetails(exchangeWithOwner: String): Flowable<Map<String, List<Balance>>>
	fun getBalanceDetails(balances: List<Balance>?): Flowable<Balance>
}