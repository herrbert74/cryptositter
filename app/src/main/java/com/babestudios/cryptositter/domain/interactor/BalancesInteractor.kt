package com.babestudios.cryptositter.domain.interactor

import android.util.Log
import com.babestudios.base.rx.SchedulerProvider
import com.babestudios.cryptositter.data.model.Exchange
import com.babestudios.cryptositter.domain.model.Balance
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors
import javax.inject.Inject

class BalancesInteractor @Inject constructor(
		private var binanceRepository: BinanceBalancesInteractor,
		private var bittrexRepository: BittrexBalancesInteractor,
		private var bitfinexRepository: BitfinexBalancesInteractor,
		private var ftxRepository: FtxBalancesInteractor,
		private var schedulerProvider: SchedulerProvider,
) {


	fun getIncompleteBalancesFlowable(exchangeNamesWithOwners: MutableSet<String>?): Flowable<Map<String, List<Balance>>> {
		val exchangeObservables = getBalanceObservables(exchangeNamesWithOwners)
		return Flowable
				.range(0, exchangeObservables.size)
				.parallel(200)
				.runOn(Schedulers.newThread())
				.map { exchangeObservables[it].blockingFirst() }
				.sequentialDelayError()
	}

	fun getBalanceDetailsFlowable(incompleteBalancesFlowable: Flowable<Map<String, List<Balance>>>): Flowable<Balance> {
		return incompleteBalancesFlowable.flatMap { balancesForExchange ->
			Flowable
					.just(balancesForExchange)
					.subscribeOn(Schedulers.from(Executors.newSingleThreadExecutor()))
					.flatMap { getBalanceDetailObservable(balancesForExchange) }
		}
				.compose(schedulerProvider.getSchedulersForFlowable())
	}

	private fun getBalanceObservables(exchangeNamesWithOwners: MutableSet<String>?): List<Flowable<Map<String, List<Balance>>>> {
		var result = emptyList<Flowable<Map<String, List<Balance>>>>()
		exchangeNamesWithOwners?.forEach { exchangeWithOwner ->
			Log.d("logCatText", "getBalanceObservables: $exchangeWithOwner ${Exchange.BITTREX.key}")
			result = when {
				exchangeWithOwner.startsWith(Exchange.BINANCE.key, true) ->
					result.plus(binanceRepository.getBalancesWithDetails(exchangeWithOwner))
				exchangeWithOwner.startsWith(Exchange.BITTREX.key, true) ->
					result.plus(bittrexRepository.getBalancesWithDetails(exchangeWithOwner))
				exchangeWithOwner.startsWith(Exchange.BITFINEX.key, true) ->
					result.plus(bitfinexRepository.getBalancesWithDetails(exchangeWithOwner))
				exchangeWithOwner.startsWith(Exchange.FTX.key, true) ->
					result.plus(ftxRepository.getBalancesWithDetails(exchangeWithOwner))
				else -> result.plus(emptyList())
			}
		}
		return result
	}

	/**
	 * Get the ticker prices, 24h changes for an exchange.
	 */
	private fun getBalanceDetailObservable(exchange: Map<String, List<Balance>>): Flowable<Balance> {
		val balanceKey = ArrayList<String>(exchange.keys)[0]
		return when {
			balanceKey.startsWith(Exchange.BINANCE.key, true) -> binanceRepository.getBalanceDetails(exchange[balanceKey])
			balanceKey.startsWith(Exchange.BITTREX.key, true) -> bittrexRepository.getBalanceDetails(exchange[balanceKey])
			balanceKey.startsWith(Exchange.BITFINEX.key, true) -> bitfinexRepository.getBalanceDetails(exchange[balanceKey])
			balanceKey.startsWith(Exchange.FTX.key, true) -> ftxRepository.getBalanceDetails(exchange[balanceKey])
			else -> Flowable.empty()
		}
	}

	fun clearCache() {
		binanceRepository.clearCache()
		bittrexRepository.clearCache()
		bitfinexRepository.clearCache()
		bitfinexRepository.clearCache()
	}
}