package com.babestudios.cryptositter.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Balance(
		val exchange: String,
		val symbol: String,
		val currencyName: String,
		val amount: Double,
		val lastUsdPrice: Double,
		var lastPrice: Double = 0.0,
		var dailyChange: Double = 0.0,
		var dailyChangePercentage: Double = 0.0
) : Parcelable
