package com.babestudios.cryptositter.domain.interactor

import com.babestudios.cryptositter.data.model.binance.Binance24hChangeResponse
import com.babestudios.cryptositter.data.model.binance.BinanceBalancesResponse
import com.babestudios.cryptositter.data.model.binance.BinanceGetTickerResponse
import com.babestudios.cryptositter.data.network.base.KEY_CACHE_AGE_LOWER_LIMIT
import com.babestudios.cryptositter.data.network.binance.BinanceCache
import com.babestudios.cryptositter.data.network.binance.BinanceRepository
import com.babestudios.cryptositter.domain.model.Balance
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.rxkotlin.Flowables
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.Executors
import javax.inject.Inject


class BinanceBalancesInteractor @Inject constructor(
		private var binanceCache: BinanceCache,
		private var binanceRepository: BinanceRepository
) : ExchangeInteractor {

	//region complex calls

	override fun getBalancesWithDetails(exchangeWithOwner: String): Flowable<Map<String, List<Balance>>> {
		return Flowables.combineLatest(
				getBalancesAndCacheIt(exchangeWithOwner).subscribeOn(Schedulers.newThread()).toFlowable()
						.onErrorResumeNext(
								Flowable.just(
										BinanceBalancesResponse(
												ArrayList(),
												canTrade = false,
												canWithdraw = false,
												canDeposit = false,
												makerCommission = 0,
												takerCommission = 0,
												buyerCommission = 0,
												sellerCommission = 0,
												updateTime = 0
										))),
				getTickerAndCacheIt("BTCUSDT").subscribeOn(Schedulers.newThread()).toFlowable()
		) { response, btcUsdLastPrice ->
			response.balances.forEach { it.lastBtcUsdPrice = btcUsdLastPrice.price }
			response.balances
		}
				.flatMapIterable { response -> response }
				.map { item -> item.convertToBalance() }
				.filter { balance -> balance.amount > 0.0 }
				.toList()
				.map { t -> mapOf(exchangeWithOwner to t) }
				.toFlowable()
	}

	override fun getBalanceDetails(balances: List<Balance>?): Flowable<Balance> {
		return Flowable.fromArray(balances)
				.flatMapIterable { response -> response }
				.flatMap({ balance ->
					Flowables.combineLatest(
							getTickerAndCacheIt(binanceRepository.getBitcoinTickerSymbol(balance.symbol))
									.subscribeOn(Schedulers.newThread()).toFlowable(),
							get24hChangeAndCacheIt(binanceRepository.getBitcoinTickerSymbol(balance.symbol))
									.subscribeOn(Schedulers.newThread()).toFlowable()
					) { ticker, marketSummary ->
						addDetailsToBalance(balance, ticker, marketSummary)
					}
							.subscribeOn(Schedulers.from(Executors.newCachedThreadPool()))
							.onErrorResumeNext { _: Throwable ->
								balance.lastPrice = Double.NaN
								Flowable.just(balance)
							}
				}, { balance, _ ->
					balance
				})
	}

	//endregion

	//region atomic calls

	//TODO Switch caching
	/*private fun getBalancesResponse(): Flowable<BinanceBalancesResponse> {
		return binanceCache.getBalancesResponse()
				.switchIfEmpty(getBalancesAndCacheIt(exchangeWithOwner))
				.toFlowable()
				.doOnNext {
					if (Date(it.updateTime).before(KEY_CACHE_AGE_LOWER_LIMIT)) {
						getBalancesAndCacheIt(exchangeWithOwner).subscribe()
					}
				}
	}*/

	private fun getBalancesAndCacheIt(exchangeWithOwner: String): Single<BinanceBalancesResponse> {
		return binanceRepository.getBinanceBalancesResponse(exchangeWithOwner)
				.firstOrError()
				.doOnSuccess {
					binanceCache.putBalancesResponse(it)
				}
	}

	private fun getTickerResponse(symbol: String): Flowable<BinanceGetTickerResponse> {
		return binanceCache.getTickerResponse(symbol)
				.switchIfEmpty(getTickerAndCacheIt(symbol))
				.toFlowable()
				.doOnNext {
					if (Date(it.updateTime).before(KEY_CACHE_AGE_LOWER_LIMIT)) {
						getTickerAndCacheIt(symbol)
								.onErrorResumeNext(Single.just(BinanceGetTickerResponse()))
								.subscribe()
					}
				}
	}

	private fun getTickerAndCacheIt(symbol: String): Single<BinanceGetTickerResponse> {
		return binanceRepository.getBinanceTicker(symbol)
				.firstOrError()
				.doOnSuccess {
					binanceCache.putTickerResponse(it, symbol)
				}
	}

	private fun get24hChangeResponse(symbol: String): Flowable<Binance24hChangeResponse> {
		return binanceCache.get24hChangeResponse(symbol)
				.switchIfEmpty(get24hChangeAndCacheIt(symbol))
				.toFlowable()
				.doOnNext {
					if (Date(it.updateTime).before(KEY_CACHE_AGE_LOWER_LIMIT)) {
						get24hChangeAndCacheIt(symbol).subscribe()
					}
				}
	}

	private fun get24hChangeAndCacheIt(symbol: String): Single<Binance24hChangeResponse> {
		return binanceRepository.getBinance24hChange(symbol)
				.firstOrError()
				.doOnSuccess {
					binanceCache.put24hResponse(it, symbol)
				}
	}
	//endregion

	//region utility

	private fun addDetailsToBalance(balance: Balance, ticker: BinanceGetTickerResponse, change: Binance24hChangeResponse): Balance {
		balance.lastPrice = ticker.price
		balance.dailyChange = change.priceChange
		balance.dailyChangePercentage = change.priceChangePercent
		return balance
	}

	fun clearCache() {
		binanceCache.clearResponses()
	}

	//endregion
}
