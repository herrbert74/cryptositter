package com.babestudios.cryptositter

import com.babestudios.cryptositter.data.network.base.CryptoRepository
import com.babestudios.cryptositter.data.network.binance.BinanceRepository
import com.babestudios.cryptositter.data.network.bitfinex.BitfinexRepository
import com.babestudios.cryptositter.data.network.bittrex.BittrexRepository
import com.babestudios.cryptositter.data.network.ftx.FtxRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface DataContractModule {

	@Singleton
	@Binds
	fun bindBinanceRepository(binanceRepository: BinanceRepository): CryptoRepository

	@Singleton
	@Binds
	fun bindBittrexRepository(bittrexRepository: BittrexRepository): CryptoRepository

	@Singleton
	@Binds
	fun bindBitfinexRepository(bitfinexRepository: BitfinexRepository): CryptoRepository

	@Singleton
	@Binds
	fun bindFtxRepository(ftxRepository: FtxRepository): CryptoRepository

}