package com.babestudios.cryptositter.settings

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.babestudios.cryptositter.R
import com.babestudios.cryptositter.apikeys.model.ApiKeyModel
import com.babestudios.cryptositter.databinding.DialogEditExchangeBinding
import java.util.*

class EditExchangeDialogFragment : DialogFragment() {

	@SuppressLint("InflateParams")
	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		val builder = AlertDialog.Builder(requireActivity())
		val binding = DialogEditExchangeBinding.inflate(LayoutInflater.from(context))
		binding.lblEditExchangeTitle.text = exchange?.first
		binding.etApiKey.setText(exchange?.second?.apiKey)
		binding.etApiSecret.setText(exchange?.second?.apiSecret)

		builder.setView(view)
		builder.setTitle(getString(R.string.edit_exchange))
				.setPositiveButton(resources.getString(R.string.save).uppercase(Locale.getDefault())) { _: DialogInterface, _: Int ->
				}
				.setNegativeButton(resources.getString(R.string.delete).uppercase(Locale.getDefault()), null)
		val dialog = builder.create()
		dialog.setOnShowListener {
			val btnSave = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
			btnSave.setOnClickListener {
				listener?.editExchange(
						exchange?.first,
						binding.etApiKey.text.toString(),
						binding.etApiSecret.text.toString()
				)
				dialog.dismiss()
			}
			val btnDelete = dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
			btnDelete.setOnClickListener {
				listener?.deleteExchange(exchange?.first)
				dialog.dismiss()
			}
		}
		return dialog
	}

	interface EditExchangeDialogListener {
		fun editExchange(exchangeAndOwner: String?, apiKey: String, apiSecret: String)
		fun deleteExchange(exchange: String?)
	}

	companion object {

		private var listener: EditExchangeDialogListener? = null

		private var exchange: Pair<String, ApiKeyModel>? = null

		fun newInstance(
				listener: EditExchangeDialogListener,
				exchange: Pair<String, ApiKeyModel>?
		): EditExchangeDialogFragment {
			EditExchangeDialogFragment.listener = listener
			EditExchangeDialogFragment.exchange = exchange
			return EditExchangeDialogFragment()
		}
	}


}
