package com.babestudios.cryptositter.settings

import com.babestudios.cryptositter.apikeys.model.ApiKeyModel

data class SettingsModel(var apiKeyMap: HashMap<String, ApiKeyModel>?, var showSmallAmountsOnly: Boolean)
