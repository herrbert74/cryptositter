package com.babestudios.cryptositter.settings

import com.babestudios.base.mvp.BaseView
import com.babestudios.cryptositter.balances.AddExchangeDialogFragment

interface SettingsView : BaseView, AddExchangeDialogFragment.AddExchangeDialogListener, EditExchangeDialogFragment.EditExchangeDialogListener {

	fun showSettings(model: SettingsModel?)

}