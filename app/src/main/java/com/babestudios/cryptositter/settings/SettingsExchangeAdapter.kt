package com.babestudios.cryptositter.settings

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.babestudios.cryptositter.R
import com.babestudios.cryptositter.apikeys.model.ApiKeyModel


class SettingsExchangeAdapter (private var apiKeyMap: LinkedHashMap<String, ApiKeyModel>?, context: Context) : RecyclerView.Adapter<SettingsExchangeViewHolder>()
{
	private val settingsActivity: SettingsActivity = context as SettingsActivity
	private val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsExchangeViewHolder {
		val view = inflater.inflate(R.layout.list_item_settings_exchange, parent, false)
		return SettingsExchangeViewHolder(view)
	}

	override fun getItemCount(): Int {
		return if (apiKeyMap != null) apiKeyMap!!.size else 0
	}

	override fun onBindViewHolder(holder: SettingsExchangeViewHolder, position: Int) {
		apiKeyMap?.let {
			holder.setExchangeAndApiKey(ArrayList<String>(it.keys)[position], ArrayList<ApiKeyModel>(it.values)[position])
		}
		holder.itemView.setOnClickListener {
			val dialog = EditExchangeDialogFragment.newInstance(settingsActivity, apiKeyMap?.toList()?.get(position))
			dialog.show(settingsActivity.supportFragmentManager, "EditExchange")
		}
	}

	fun addExchange(exchangeAndOwner: String?, apiKey: String, apiSecret: String) {
		exchangeAndOwner?.let { apiKeyMap?.put(it, ApiKeyModel(apiKey, apiSecret)) }
		notifyDataSetChanged()
	}

	fun deleteExchange(exchangeAndOwner: String?) {
		apiKeyMap?.remove(exchangeAndOwner)
		notifyDataSetChanged()
	}
}