package com.babestudios.cryptositter.settings

import com.babestudios.base.ext.DelegatesExt
import com.babestudios.base.mvp.BaseRxPresenter
import com.babestudios.cryptositter.CryptoSitterApplication
import com.babestudios.cryptositter.apikeys.ApiKeyRepository
import com.babestudios.cryptositter.apikeys.model.ApiKeyModel


class SettingsPresenter : BaseRxPresenter<SettingsModel, SettingsView>() {

	private var smallAmount : Boolean by DelegatesExt.preference(CryptoSitterApplication.context, "SMALL_AMOUNT", false)

	override fun bindView(view: SettingsView) {
		super.bindView(view)
		model = SettingsModel(ApiKeyRepository.loadApiKeys(CryptoSitterApplication.context), smallAmount)
		view.showSettings(model)
	}

	override fun updateView() {

	}

	fun setShowSmallAmounts(showSmallAmounts : Boolean) {
		smallAmount = showSmallAmounts
		model?.showSmallAmountsOnly = showSmallAmounts
	}

	fun exchangeAdded(exchangeAndOwner: String?, apiKey: String, apiSecret: String) {
		ApiKeyRepository.saveApiKey(CryptoSitterApplication.context, exchangeAndOwner, apiKey, apiSecret)
		exchangeAndOwner?.let { model?.apiKeyMap?.put(it, ApiKeyModel(apiKey, apiSecret)) }
	}

	fun deleteExchange(exchangeAndOwner: String?) {
		ApiKeyRepository.deleteApiKey(CryptoSitterApplication.context, exchangeAndOwner)
		model?.apiKeyMap?.remove(exchangeAndOwner)
	}

}
