package com.babestudios.cryptositter.settings

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.babestudios.cryptositter.apikeys.model.ApiKeyModel
import com.babestudios.cryptositter.databinding.ListItemSettingsExchangeBinding


class SettingsExchangeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

	val binding = ListItemSettingsExchangeBinding.bind(itemView)

	fun setExchangeAndApiKey(exchange: String, apiKeyModel: ApiKeyModel) {
		binding.lblSettingsExchange.text = exchange
		binding.lblSettingsExchangeApiKey.text = apiKeyModel.apiKey
	}
}