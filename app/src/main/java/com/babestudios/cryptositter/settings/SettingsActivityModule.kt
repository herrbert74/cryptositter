package com.babestudios.cryptositter.settings

import dagger.Module
import dagger.Provides

@Module
class SettingsActivityModule {

	@Provides
	fun provideSettingsPresenter() = SettingsPresenter()
}