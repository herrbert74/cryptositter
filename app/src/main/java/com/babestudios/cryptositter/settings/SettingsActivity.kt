package com.babestudios.cryptositter.settings

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.babestudios.base.ext.viewBinding
import com.babestudios.cryptositter.R
import com.babestudios.cryptositter.balances.AddExchangeDialogFragment
import com.babestudios.cryptositter.databinding.ActivitySettingsBinding
import dagger.android.AndroidInjection
import javax.inject.Inject


class SettingsActivity : AppCompatActivity(), SettingsView {

	@Inject
	lateinit var settingsPresenter: SettingsPresenter

	private val binding by viewBinding(ActivitySettingsBinding::inflate)

	private var layoutManager: LinearLayoutManager? = null

	lateinit var adapter: SettingsExchangeAdapter

	override fun onCreate(savedInstanceState: Bundle?) {
		AndroidInjection.inject(this)
		super.onCreate(savedInstanceState)
		setContentView(binding.root)
		setSupportActionBar(binding.toolbarSettings)
	}

	override fun onResume() {
		settingsPresenter.bindView(this)
		super.onResume()
	}

	override fun onPause() {
		settingsPresenter.unbindView()
		super.onPause()
	}

	override fun showSettings(model: SettingsModel?) {
		layoutManager = LinearLayoutManager(this@SettingsActivity)
		binding.recyclerSettingsExchanges.layoutManager = layoutManager
		adapter = SettingsExchangeAdapter(LinkedHashMap(model?.apiKeyMap), this)
		binding.recyclerSettingsExchanges.adapter = adapter

		binding.cbSmallAmounts.isChecked = model?.showSmallAmountsOnly!!
		binding.cbSmallAmounts.setOnClickListener {
			settingsPresenter.setShowSmallAmounts(binding.cbSmallAmounts.isChecked)
		}
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		// Inflate the menu; this adds items to the action bar if it is present.
		val menuInflater = menuInflater
		menuInflater.inflate(R.menu.menu_settings, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return when (item.itemId) {
			R.id.action_add_exchange -> {
				val dialog = AddExchangeDialogFragment.newInstance(this)
				dialog.show(supportFragmentManager, "AddExchange")
				true
			}
			else -> super.onOptionsItemSelected(item)
		}
	}

	override fun editExchange(exchangeAndOwner: String?, apiKey: String, apiSecret: String) {
		settingsPresenter.exchangeAdded(exchangeAndOwner, apiKey, apiSecret)
		adapter.addExchange(exchangeAndOwner, apiKey, apiSecret)
	}

	override fun addExchange(exchangeAndOwner: String, apiKey: String, apiSecret: String) {
		settingsPresenter.exchangeAdded(exchangeAndOwner, apiKey, apiSecret)
		adapter.addExchange(exchangeAndOwner, apiKey, apiSecret)
	}

	override fun deleteExchange(exchange: String?) {
		settingsPresenter.deleteExchange(exchange)
		adapter.deleteExchange(exchange)
	}

	override fun onTimeout() {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun onNetworkError() {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun showProgress() {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun hideProgress() {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun onUnknownError(errorMessage: String) {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

}