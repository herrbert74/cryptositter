package com.babestudios.cryptositter

import com.babestudios.base.rx.SchedulerProvider
import com.babestudios.cryptositter.data.network.binance.BinanceService
import com.babestudios.cryptositter.data.network.bitfinex.BitfinexService
import com.babestudios.cryptositter.data.network.bittrex.BittrexService
import com.babestudios.cryptositter.data.network.converters.AdvancedGsonConverterFactory
import com.babestudios.cryptositter.data.network.converters.BooleanSerializer
import com.babestudios.cryptositter.data.network.converters.DateSerializer
import com.babestudios.cryptositter.data.network.converters.StringSerializer
import com.babestudios.cryptositter.data.network.cryptocompare.CryptoCompareService
import com.babestudios.cryptositter.data.network.ftx.FtxService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule {

	//region basics

	@Provides
	@Singleton
	fun provideSchedulerProvider() = SchedulerProvider(
			Schedulers.from(Executors.newCachedThreadPool()),
			AndroidSchedulers.mainThread()
	)

	@Provides
	@Singleton
	fun provideGson(): Gson {
		return GsonBuilder()//
				.registerTypeAdapter(Date::class.java, DateSerializer())//
				.registerTypeAdapter(Boolean::class.java, BooleanSerializer())//
				.registerTypeAdapter(Boolean::class.javaPrimitiveType, BooleanSerializer())//
				.registerTypeAdapter(String::class.java, StringSerializer())//
				.create()
	}

	//endregion

	//region httpClients

	private fun getBaseHttpClientBuilder(): OkHttpClient.Builder {
		val okClientBuilder = OkHttpClient.Builder()

		//Timeouts
		okClientBuilder.connectTimeout(10, TimeUnit.SECONDS)
		okClientBuilder.readTimeout(20, TimeUnit.SECONDS)
		okClientBuilder.writeTimeout(20, TimeUnit.SECONDS)

		//Logging
		val httpLoggingInterceptor = HttpLoggingInterceptor()
		httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

		okClientBuilder
				.addInterceptor(ChuckInterceptor(CryptoSitterApplication.context))
				.networkInterceptors().add(httpLoggingInterceptor)

		return okClientBuilder
	}

	private fun getBaseHttpClient(): OkHttpClient {
		return getBaseHttpClientBuilder().build()
	}

	//endregion

	//region Retrofit

	@Provides
	@Singleton
	@Named("BaseRetrofitBuilder")
	fun provideBaseRetrofitBuilder(advancedGsonConverterFactory: AdvancedGsonConverterFactory): Retrofit.Builder {
		val builder = Retrofit.Builder()
		return builder
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.addConverterFactory(advancedGsonConverterFactory)
	}

	@Provides
	@Singleton
	@Named("CryptoCompareRetrofit")
	fun provideCryptoCompareRetrofit(
			@Named("BaseRetrofitBuilder") baseRetrofitBuilder: Retrofit.Builder
	): Retrofit {
		return baseRetrofitBuilder
				.client(getBaseHttpClient())
				.baseUrl(BuildConfig.CRYPTOCOMPARE_BASE_URL)
				.build()
	}

	@Provides
	@Singleton
	@Named("BinanceRetrofit")
	fun provideBinanceRetrofit(
			@Named("BaseRetrofitBuilder") baseRetrofitBuilder: Retrofit.Builder
	): Retrofit {
		return baseRetrofitBuilder
				.client(getBaseHttpClient())
				.baseUrl(BuildConfig.BINANCE_BASE_URL)
				.build()
	}

	@Provides
	@Singleton
	@Named("BittrexRetrofit")
	fun provideBittrexRetrofit(
			@Named("BaseRetrofitBuilder") baseRetrofitBuilder: Retrofit.Builder
	): Retrofit {
		return baseRetrofitBuilder
				.client(getBaseHttpClient())
				.baseUrl(BuildConfig.BITTREX_BASE_URL)
				.build()
	}

	@Provides
	@Singleton
	@Named("BitfinexRetrofit")
	fun provideBitfinexRetrofit(
			@Named("BaseRetrofitBuilder") baseRetrofitBuilder: Retrofit.Builder
	): Retrofit {
		return baseRetrofitBuilder
				.client(getBaseHttpClient())
				.baseUrl(BuildConfig.BITFINEX_BASE_URL)
				.build()
	}

	@Provides
	@Singleton
	@Named("FtxRetrofit")
	fun provideFtxRetrofit(
			@Named("BaseRetrofitBuilder") baseRetrofitBuilder: Retrofit.Builder
	): Retrofit {
		return baseRetrofitBuilder
				.client(getBaseHttpClient())
				.baseUrl(BuildConfig.FTX_BASE_URL)
				.build()
	}

	//endregion

	//region Retrofit Services

	@Provides
	@Singleton
	fun provideCryptoCompareService(@Named("CryptoCompareRetrofit") cryptoCompareRetrofit: Retrofit): CryptoCompareService {
		return cryptoCompareRetrofit.create(CryptoCompareService::class.java)
	}

	@Provides
	@Singleton
	fun provideBinanceService(@Named("BinanceRetrofit") binanceRetrofit: Retrofit): BinanceService {
		return binanceRetrofit.create(BinanceService::class.java)
	}

	@Provides
	@Singleton
	fun provideBittrexService(@Named("BittrexRetrofit") bittrexRetrofit: Retrofit): BittrexService {
		return bittrexRetrofit.create(BittrexService::class.java)
	}

	@Provides
	@Singleton
	fun provideBitfinexService(@Named("BitfinexRetrofit") bitfinexRetrofit: Retrofit): BitfinexService {
		return bitfinexRetrofit.create(BitfinexService::class.java)
	}

	@Provides
	@Singleton
	fun provideFtxService(@Named("FtxRetrofit") ftxRetrofit: Retrofit): FtxService {
		return ftxRetrofit.create(FtxService::class.java)
	}

	//endregion

}
