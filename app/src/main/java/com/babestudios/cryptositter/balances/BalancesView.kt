package com.babestudios.cryptositter.balances

import com.babestudios.cryptositter.balances.model.ExchangeExpandableGroup
import com.babestudios.base.mvp.BaseView
import com.babestudios.cryptositter.domain.model.Balance
import io.reactivex.Observable

interface BalancesView : BaseView {

	fun updateHeaderWithBalances(btcBalance: Double, usdBalance: Double)

	fun updateExchangeData(updatedExchangeData: ExchangeExpandableGroup)

	fun scrollToPosition(exchange: String)

	fun startChartActivity(exchange: String, symbol: String)

	fun showExchangesLoadingProgress(exchangeNames: MutableSet<String>?)

	fun updateBalanceData(updatedBalanceData: Balance)
	fun getMenuRefreshObservable(): Observable<Any>
}