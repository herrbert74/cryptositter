package com.babestudios.cryptositter.balances

import com.babestudios.cryptositter.balances.model.ExchangeExpandableGroup

data class BalancesModel(var exchanges: Map<String, ExchangeExpandableGroup>)