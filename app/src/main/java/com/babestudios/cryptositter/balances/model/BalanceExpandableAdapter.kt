package com.babestudios.cryptositter.balances.model

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.babestudios.base.views.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.babestudios.base.views.expandablerecyclerview.models.ExpandableGroup
import com.babestudios.base.views.expandablerecyclerview.models.ExpandableList
import com.babestudios.cryptositter.R
import com.babestudios.cryptositter.data.CryptoMaps
import com.babestudios.cryptositter.domain.model.Balance
import java.util.*


class BalanceExpandableAdapter(groups: List<ExpandableGroup<*>>, context: Context)
	: ExpandableRecyclerViewAdapter<ExchangeViewHolder, BalanceViewHolder>(groups) {

	private val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

	override fun onCreateGroupViewHolder(parent: ViewGroup, viewType: Int): ExchangeViewHolder {
		val view = inflater.inflate(R.layout.list_item_balances_exchange, parent, false)
		return ExchangeViewHolder(view)
	}

	override fun onCreateChildViewHolder(parent: ViewGroup, viewType: Int): BalanceViewHolder {
		val view = inflater.inflate(R.layout.list_item_balances_balance, parent, false)
		return BalanceViewHolder(view)
	}

	override fun onBindChildViewHolder(holder: BalanceViewHolder, flatPosition: Int, group: ExpandableGroup<*>, childIndex: Int) {
		val balance = (group as ExchangeExpandableGroup).items[childIndex]
		holder.binding.ivBalanceItemLogo.setImageResource(CryptoMaps.logoMap.getValue(balance.symbol.lowercase(Locale.getDefault())))
		holder.updateHolder(balance)
	}

	override fun onBindGroupViewHolder(holder: ExchangeViewHolder, flatPosition: Int, group: ExpandableGroup<*>) {
		holder.setBalancesItemExchange(group as ExchangeExpandableGroup)
		if (group.items.size == 0 || group.items[0]?.exchange != "empty") {
			holder.hideProgress()
		}
	}

	fun getFlattenedGroupPosition(exchange: String): Int {
		return expandableList.getFlattenedGroupIndex(groups.indexOfFirst { it.title == exchange })
	}

	private fun getGroupPosition(exchange: String): Int {
		return groups.indexOfFirst { it.title.startsWith(exchange) }
	}

	fun updateExchangeData(updatedExchangeData: ExchangeExpandableGroup) {
		expandableList.updateGroup(updatedExchangeData)
		notifyItemChanged(getFlattenedGroupPosition(updatedExchangeData.title))
	}

	private fun getBalancePosition(balance: Balance): Int {
		return expandableList.getFlattenedChildIndex(
				getGroupPosition(balance.exchange),
				groups[getGroupPosition(balance.exchange)].items.indexOf(balance)
		)
	}

	fun updateBalanceData(updatedBalance: Balance) {
		expandableList.updateBalance(updatedBalance)
		notifyItemChanged(getBalancePosition(updatedBalance))
	}
}

private fun ExpandableList.updateBalance(updatedBalance: Balance) {
	val exchange = groups[groups.indexOfFirst { it.title.startsWith(updatedBalance.exchange) }]
	if (exchange.items.indexOf(updatedBalance) != -1) {
		exchange.items[exchange.items.indexOf(updatedBalance)] = updatedBalance
	}
}
