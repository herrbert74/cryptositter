package com.babestudios.cryptositter.balances

import android.util.Log
import com.babestudios.base.mvp.BaseRxPresenter
import com.babestudios.base.rx.RxBus
import com.babestudios.base.rx.SchedulerProvider
import com.babestudios.cryptositter.apikeys.ApiKeyRepository
import com.babestudios.cryptositter.balances.model.ExchangeExpandableGroup
import com.babestudios.cryptositter.domain.interactor.*
import com.babestudios.cryptositter.domain.model.Balance
import io.reactivex.functions.Consumer
import io.reactivex.subscribers.ResourceSubscriber
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap


class BalancesPresenter @Inject constructor(
		private var balancesInteractor: BalancesInteractor,
		private var schedulerProvider: SchedulerProvider,
) : BaseRxPresenter<BalancesModel, BalancesView>() {

	override fun bindView(view: BalancesView) {
		super.bindView(view)
		RxBus.subscribe(RxBus.SUBJECT_BALANCES_EXCHANGE_CLICKED, this, exchangeClickedConsumer)
		RxBus.subscribe(RxBus.SUBJECT_BALANCES_BALANCE_CLICKED, this, balanceClickedConsumer)
		loadBalances()
	}

	private val exchangeClickedConsumer = Consumer<Any> { exchange ->
		run {
			if (exchange is String) {
				view()?.scrollToPosition(exchange)
			}
		}
	}

	private val balanceClickedConsumer = Consumer<Any> { balance ->
		run {
			if (balance is Balance) {
				view()?.startChartActivity(balance.exchange, balance.symbol)
			}
		}
	}

	override fun updateView() {

	}

	fun loadBalances() {
		disposables.clear()
		val exchangeNamesWithOwners = ApiKeyRepository.getExchangeNamesWithApiKeys()
		view()?.showExchangesLoadingProgress(exchangeNamesWithOwners)
		model = BalancesModel(HashMap())
		val incompleteBalancesFlowable = balancesInteractor.getIncompleteBalancesFlowable(exchangeNamesWithOwners)
				.doOnNext {
					val exchangeExpandableGroup = ExchangeExpandableGroup(it.keys.first(), it.values.first())
					model?.exchanges = model?.exchanges?.plus(Pair(it.keys.first(), exchangeExpandableGroup))!!
					Timber.d("loadBalances: incomplete")
					updateHeaderWithBalances()
					view()?.updateExchangeData(exchangeExpandableGroup)

				}

		disposables.add(balancesInteractor.getBalanceDetailsFlowable(incompleteBalancesFlowable)
				.subscribeWith(object : ResourceSubscriber<Balance>() {
					override fun onComplete() {
						Log.d("logCatText", "onComplete: ")
					}

					override fun onNext(balance: Balance) {
						Timber.d("loadBalances onNext: $balance")
						updateHeaderWithBalances()
						view()?.updateBalanceData(balance)
					}

					override fun onError(e: Throwable) {
						Log.e("logCatText", "onError: ${e.localizedMessage}")
					}
				}))
	}

	private fun updateHeaderWithBalances() {
		var btcBalance = 0.0
		var usdBalance = 0.0
		model?.exchanges?.values?.forEach { group ->
			group.items.forEach inner@{
				if (it.lastPrice.isNaN()) return@inner
				btcBalance += if (it.symbol.lowercase(Locale.ROOT) == "btc") {
					it.amount
				} else if ((it.symbol.lowercase(Locale.ROOT) == "usd" || it.symbol.lowercase(Locale.ROOT) == "usdt") && it.lastPrice > 0.0) {
					it.amount / it.lastPrice
				} else {
					it.amount * it.lastPrice
				}
				usdBalance += if (it.symbol.lowercase(Locale.ROOT) == "usd" || it.symbol.lowercase(Locale.ROOT) == "usdt") {
					it.amount
				} else if (it.symbol.lowercase(Locale.ROOT) == "btc") {
					it.amount * it.lastUsdPrice
				} else {
					it.amount * it.lastPrice * it.lastUsdPrice
				}
			}
		}
		view()?.updateHeaderWithBalances(btcBalance, usdBalance)
	}

	fun clearCache() {
		balancesInteractor.clearCache()
	}
}