package com.babestudios.cryptositter.balances.model

import com.babestudios.cryptositter.domain.model.Balance
import com.babestudios.base.views.expandablerecyclerview.models.ExpandableGroup


class ExchangeExpandableGroup(title: String, items : List<Balance>) : ExpandableGroup<Balance>(title, items)