package com.babestudios.cryptositter.balances

import com.babestudios.base.rx.SchedulerProvider
import com.babestudios.cryptositter.domain.interactor.*
import dagger.Module
import dagger.Provides

@Module
class BalancesActivityModule {

	@Provides
	fun provideBalancesPresenter(
			balancesInteractor: BalancesInteractor,
			schedulerProvider: SchedulerProvider
	) = BalancesPresenter(balancesInteractor, schedulerProvider)
}
