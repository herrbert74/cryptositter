package com.babestudios.cryptositter.balances

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.babestudios.cryptositter.BuildConfig
import com.babestudios.cryptositter.R
import com.babestudios.cryptositter.databinding.DialogAddExchangeBinding
import java.util.Locale

class AddExchangeDialogFragment : DialogFragment() {

	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		val binding = DialogAddExchangeBinding.inflate(LayoutInflater.from(context))
		val builder = AlertDialog.Builder(requireActivity())
		builder.setView(binding.root)
		val spinnerArrayAdapter = ArrayAdapter(
				requireActivity(),
				android.R.layout.simple_spinner_dropdown_item,
				resources.getStringArray(R.array.list_items_add_exchange)
		)
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
		binding.spinnerAvailableExchanges.adapter = spinnerArrayAdapter
		binding.spinnerAvailableExchanges.onItemSelectedListener = object : OnItemSelectedListener {
			override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
				when (p2) {
					1 -> {
						binding.etApiKey.setText(BuildConfig.BINANCE_API_KEY, TextView.BufferType.NORMAL)
						binding.etApiSecret.setText(BuildConfig.BINANCE_API_SECRET, TextView.BufferType.NORMAL)
					}
					2 -> {
						binding.etApiKey.setText(BuildConfig.BITTREX_API_KEY, TextView.BufferType.NORMAL)
						binding.etApiSecret.setText(BuildConfig.BITTREX_API_SECRET, TextView.BufferType.NORMAL)
					}
					3 -> {
						binding.etApiKey.setText(BuildConfig.BITFINEX_API_KEY, TextView.BufferType.NORMAL)
						binding.etApiSecret.setText(BuildConfig.BITFINEX_API_SECRET, TextView.BufferType.NORMAL)
					}
					4 -> {
						binding.etApiKey.setText(BuildConfig.FTX_VERA_API_KEY, TextView.BufferType.NORMAL)
						binding.etApiSecret.setText(BuildConfig.FTX_VERA_API_SECRET, TextView.BufferType.NORMAL)
					}
					5 -> {
						binding.etApiKey.setText(BuildConfig.BINANCE_LACI_API_KEY, TextView.BufferType.NORMAL)
						binding.etApiSecret.setText(BuildConfig.BINANCE_LACI_API_SECRET, TextView.BufferType.NORMAL)
					}
					6 -> {
						binding.etApiKey.setText(BuildConfig.FTX_RAMONA_API_KEY, TextView.BufferType.NORMAL)
						binding.etApiSecret.setText(BuildConfig.FTX_RAMONA_API_SECRET, TextView.BufferType.NORMAL)
					}
					7 -> {
						binding.etApiKey.setText(BuildConfig.FTX_CSENGE_API_KEY, TextView.BufferType.NORMAL)
						binding.etApiSecret.setText(BuildConfig.FTX_CSENGE_API_SECRET, TextView.BufferType.NORMAL)
					}
				}
			}

			override fun onNothingSelected(p0: AdapterView<*>?) {
				TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
			}

		}
		builder.setTitle(getString(R.string.add_an_exchange))
				.setPositiveButton(resources.getString(R.string.add).uppercase(Locale.ROOT)) { _: DialogInterface, _: Int ->
				}
				.setNegativeButton(resources.getString(android.R.string.cancel).uppercase(Locale.ROOT), null)
		val dialog = builder.create()
		dialog.setOnShowListener {
			val button = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
			button.setOnClickListener {
				if (binding.spinnerAvailableExchanges.selectedItemPosition > 0) {
					listener!!.addExchange(
							binding.spinnerAvailableExchanges.selectedItem as String,
							binding.etApiKey.text.toString(),
							binding.etApiSecret.text.toString()
					)
					dialog.dismiss()
				}
			}
		}
		return dialog
	}

	interface AddExchangeDialogListener {
		fun addExchange(exchangeAndOwner: String, apiKey: String, apiSecret: String)
	}

	companion object {

		private var listener: AddExchangeDialogListener? = null

		fun newInstance(listener: AddExchangeDialogListener): AddExchangeDialogFragment {
			AddExchangeDialogFragment.listener = listener
			return AddExchangeDialogFragment()
		}
	}


}
