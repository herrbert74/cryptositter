package com.babestudios.cryptositter.balances.model

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.content.ContextCompat
import com.babestudios.base.rx.RxBus
import com.babestudios.base.views.expandablerecyclerview.viewholders.ChildViewHolder
import com.babestudios.cryptositter.CryptoSitterApplication
import com.babestudios.cryptositter.R
import com.babestudios.cryptositter.data.CryptoMaps
import com.babestudios.cryptositter.domain.model.Balance
import com.babestudios.cryptositter.databinding.ListItemBalancesBalanceBinding
import java.util.*
import kotlin.math.abs

class BalanceViewHolder(itemView: View) : ChildViewHolder(itemView) {

	val binding = ListItemBalancesBalanceBinding.bind(itemView)

	private fun setAssetName(balance: Balance) {
		binding.lblBalanceItemAssetSymbol.text = balance.symbol.uppercase(Locale.getDefault())
		binding.lblBalanceItemName.text =
				CryptoMaps.currencyNameMap.getValue(balance.symbol.lowercase(Locale.getDefault()))
	}

	fun updateHolder(balance: Balance) {
		setAssetName(balance)
		setBalance(balance)
		when {

			balance.lastPrice.isNaN() -> {
				binding.pgBalanceItem.visibility = GONE
				setLastBitcoinPrice(balance)
				setLastUsdPrice(balance)
				setBitcoinValue(balance)
				setUsdValue(balance)
				setUsd24hChange(balance)
				setBitcoin24hChange(balance)
				binding.root.setOnClickListener(null)
			}
			balance.lastPrice > 0.0 -> {
				binding.pgBalanceItem.visibility = GONE
				setLastBitcoinPrice(balance)
				setLastUsdPrice(balance)
				setBitcoinValue(balance)
				setUsdValue(balance)
				setUsd24hChange(balance)
				setBitcoin24hChange(balance)
				binding.root.setOnClickListener {
					RxBus.publish(RxBus.SUBJECT_BALANCES_BALANCE_CLICKED, balance)
				}
			}
			else -> binding.pgBalanceItem.visibility = VISIBLE
		}
	}

	private fun setLastUsdPrice(balance: Balance) {
		binding.lblBalanceItemLastUsdPrice.visibility = VISIBLE
		if (balance.symbol.lowercase(Locale.getDefault()) == "usd"
				|| balance.symbol.lowercase(Locale.getDefault()) == "usdt") {
			binding.lblBalanceItemLastUsdPrice.text = "-"
		} else if (balance.symbol.lowercase(Locale.getDefault()) == "btc") {
			binding.lblBalanceItemLastUsdPrice.text = String.format("$%.2f", balance.lastPrice)
		} else {
			val formattedLastUsdPrice: String = if (balance.lastPrice * balance.lastUsdPrice < 0.1) {
				String.format("%.8f", balance.lastPrice * balance.lastUsdPrice)
			} else {
				String.format("%.2f", balance.lastPrice * balance.lastUsdPrice)
			}
			binding.lblBalanceItemLastUsdPrice.text = String.format(CryptoSitterApplication.instance.resources.getString(R.string.balance_usd), formattedLastUsdPrice)
		}
	}

	private fun setLastBitcoinPrice(balance: Balance) {
		binding.lblBalanceItemLastBitcoinPrice.visibility = VISIBLE
		if (balance.symbol.lowercase(Locale.getDefault()) == "btc") {
			binding.lblBalanceItemLastBitcoinPrice.text = "-"
		} else {
			val formattedLastPrice: String = when {
				balance.symbol.lowercase(Locale.getDefault()) == "usd" -> String.format("%.8f", 1 / balance.lastPrice)
				balance.symbol.lowercase(Locale.getDefault()) == "usdt" -> String.format("%.8f", 1 / balance.lastPrice)
				balance.lastPrice < 0.1 -> String.format("%.8f", balance.lastPrice)
				else -> String.format("%.3f", balance.lastPrice)
			}
			binding.lblBalanceItemLastBitcoinPrice.text = String.format(CryptoSitterApplication.instance.resources.getString(R.string.balance_bitcoin), formattedLastPrice)
		}
	}

	private fun setBalance(balance: Balance) {
		val formattedBalance: String = if (balance.amount < 0.1) {
			String.format("%.8f", balance.amount)
		} else {
			String.format("%.3f", balance.amount)
		}
		binding.lblBalanceItemBalance.text = formattedBalance
	}

	private fun setBitcoinValue(balance: Balance) {
		binding.lblBalanceItemBitcoinValue.visibility = VISIBLE
		if (balance.symbol.lowercase(Locale.getDefault()) == "btc") {
			binding.lblBalanceItemBitcoinValue.text = "-"
		} else {
			val formattedBitcoinValue: String =
					if (balance.symbol.lowercase(Locale.getDefault()) == "usd"
							|| balance.symbol.lowercase(Locale.getDefault()) == "usdt") {
						String.format("%.8f", balance.amount / balance.lastPrice)
					} else {
						String.format("%.8f", balance.lastPrice * balance.amount)
					}
			binding.lblBalanceItemBitcoinValue.text = String.format(CryptoSitterApplication.instance.resources.getString(R.string.balance_bitcoin), formattedBitcoinValue)
		}
	}

	private fun setUsdValue(balance: Balance) {
		binding.lblBalanceItemUsdValue.visibility = VISIBLE
		if (balance.symbol.lowercase(Locale.getDefault()) == "usd"
				|| balance.symbol.lowercase(Locale.getDefault()) == "usdt") {
			binding.lblBalanceItemUsdValue.text = "-"
		} else if (balance.symbol.lowercase(Locale.getDefault()) == "btc") {
			binding.lblBalanceItemUsdValue.text = String.format("$%.2f", balance.lastPrice * balance.amount)
		} else {
			val formattedUsdValue: String = if (balance.lastPrice * balance.lastUsdPrice < 0.1) {
				String.format("%.8f", balance.lastPrice * balance.lastUsdPrice * balance.amount)
			} else {
				String.format("%.2f", balance.lastPrice * balance.lastUsdPrice * balance.amount)
			}
			binding.lblBalanceItemUsdValue.text = String.format(CryptoSitterApplication.instance.resources.getString(R.string.balance_usd), formattedUsdValue)
		}
	}

	private fun setUsd24hChange(balance: Balance) {
		binding.lblBalanceItem24hUsdChange.visibility = VISIBLE
		binding.lblBalanceItem24hUsdPercentageChange.visibility = VISIBLE
		if (balance.symbol.lowercase(Locale.getDefault()) == "usd"
				|| balance.symbol.lowercase(Locale.getDefault()) == "usdt") {
			binding.lblBalanceItem24hUsdChange.text = "-"
			binding.lblBalanceItem24hUsdPercentageChange.text = "-"
			binding.lblBalanceItem24hUsdChange.setTextColor(
					ContextCompat.getColor(CryptoSitterApplication.context, android.R.color.white))
		} else {
			binding.lblBalanceItem24hUsdChange.setTextColor(
					ContextCompat.getColor(CryptoSitterApplication.context,
							if (balance.dailyChange > 0) android.R.color.holo_green_light
							else android.R.color.holo_red_light))
			binding.lblBalanceItem24hUsdPercentageChange.setTextColor(
					ContextCompat.getColor(CryptoSitterApplication.context,
							if (balance.dailyChange > 0) android.R.color.holo_green_light
							else android.R.color.holo_red_light))
			if (balance.symbol.lowercase(Locale.getDefault()) == "btc") {
				binding.lblBalanceItem24hUsdChange.text =
						String.format(if (balance.dailyChange > 0) "$%.2f" else "-$%.2f", abs(balance.dailyChange))
				binding.lblBalanceItem24hUsdPercentageChange.text =
						String.format("%.2f%%", balance.dailyChangePercentage)

			} else {
				val formattedUsdValue = if (abs(balance.dailyChange * balance.lastUsdPrice) < 0.01) {
					String.format("%.8f", abs(balance.dailyChange * balance.lastUsdPrice))
				} else {
					String.format("%.2f", abs(balance.dailyChange * balance.lastUsdPrice))
				}
				binding.lblBalanceItem24hUsdChange.text = String.format(
						if (balance.dailyChange * balance.lastUsdPrice > 0)
							CryptoSitterApplication.instance.resources.getString(R.string.balance_usd)
						else
							CryptoSitterApplication.instance.resources.getString(R.string.balance_usd_negative),
						formattedUsdValue)
				binding.lblBalanceItem24hUsdPercentageChange.text = String.format("%.2f%%", balance.dailyChangePercentage)
			}
		}
	}

	private fun setBitcoin24hChange(balance: Balance) {
		binding.lblBalanceItem24hBitcoinChange.visibility = VISIBLE
		binding.lblBalanceItem24hBitcoinPercentageChange.visibility = VISIBLE
		if (balance.symbol.lowercase(Locale.getDefault()) == "btc") {
			binding.lblBalanceItem24hBitcoinChange.text = "-"
			binding.lblBalanceItem24hBitcoinPercentageChange.text = "-"
			binding.lblBalanceItem24hBitcoinChange.setTextColor(ContextCompat.getColor(CryptoSitterApplication.context, android.R.color.white))
		} else {
			if (balance.dailyChange > 0) {
				binding.lblBalanceItem24hBitcoinChange.setTextColor(ContextCompat.getColor(CryptoSitterApplication.context, android.R.color.holo_green_light))
				binding.lblBalanceItem24hBitcoinPercentageChange.setTextColor(ContextCompat.getColor(CryptoSitterApplication.context, android.R.color.holo_green_light))
			} else {
				binding.lblBalanceItem24hBitcoinChange.setTextColor(ContextCompat.getColor(CryptoSitterApplication.context, android.R.color.holo_red_light))
				binding.lblBalanceItem24hBitcoinPercentageChange.setTextColor(ContextCompat.getColor(CryptoSitterApplication.context, android.R.color.holo_red_light))
			}
			val formattedBitcoinValue: String =
					if (balance.symbol.lowercase(Locale.getDefault()) == "usd"
							|| balance.symbol.lowercase(Locale.getDefault()) == "usdt") {
						val prevDay = balance.lastPrice - balance.dailyChange
						String.format("%.6f", (1 / balance.lastPrice) - (1 / prevDay))
					} else {
						String.format("%.8f", abs(balance.dailyChange))
					}
			binding.lblBalanceItem24hBitcoinChange.text = String.format(
					if (balance.dailyChange > 0)
						CryptoSitterApplication.instance.resources.getString(R.string.balance_bitcoin)
					else
						CryptoSitterApplication.instance.resources.getString(R.string.balance_bitcoin_negative),
					formattedBitcoinValue)
			binding.lblBalanceItem24hBitcoinPercentageChange.text = String.format("%.2f%%", balance.dailyChangePercentage)
		}
	}

}