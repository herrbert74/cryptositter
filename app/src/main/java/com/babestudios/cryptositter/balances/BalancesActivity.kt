package com.babestudios.cryptositter.balances

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.babestudios.base.ext.viewBinding
import com.babestudios.base.views.CardItemDecoration
import com.babestudios.cryptositter.R
import com.babestudios.cryptositter.apikeys.ApiKeyRepository
import com.babestudios.cryptositter.balances.model.BalanceExpandableAdapter
import com.babestudios.cryptositter.balances.model.ExchangeExpandableGroup
import com.babestudios.cryptositter.charts.ChartActivity
import com.babestudios.cryptositter.domain.model.Balance
import com.babestudios.cryptositter.databinding.ActivityBalancesBinding
import com.babestudios.cryptositter.settings.SettingsActivity
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding2.view.RxMenuItem
import dagger.android.AndroidInjection
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

class BalancesActivity : AppCompatActivity(), AddExchangeDialogFragment.AddExchangeDialogListener, BalancesView {

	private val binding by viewBinding(ActivityBalancesBinding::inflate)

	override fun addExchange(exchangeAndOwner: String, apiKey: String, apiSecret: String) {
		balancesPresenter.loadBalances()
		ApiKeyRepository.saveApiKey(this, exchangeAndOwner, apiKey, apiSecret)
	}

	private val eventDisposables: CompositeDisposable = CompositeDisposable()

	@Inject
	lateinit var balancesPresenter: BalancesPresenter

	private var layoutManager: LinearLayoutManager? = null

	private var adapter: BalanceExpandableAdapter? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		AndroidInjection.inject(this)
		super.onCreate(savedInstanceState)
		setContentView(binding.root)
		setSupportActionBar(binding.toolbar)
		setupRecyclerView()
		binding.fab.setOnClickListener { view ->
			Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
					.setAction("Action", null).show()
		}

	}

	private fun setupRecyclerView() {
		layoutManager = LinearLayoutManager(this@BalancesActivity)
		binding.recyclerBalance.layoutManager = layoutManager
		if (binding.recyclerBalance.itemDecorationCount > 0)
			binding.recyclerBalance.removeItemDecorationAt(0)
		binding.recyclerBalance.addItemDecoration(CardItemDecoration())

	}

	override fun onResume() {
		balancesPresenter.bindView(this)
		super.onResume()
	}

	override fun onPause() {
		balancesPresenter.unbindView()
		super.onPause()
	}

	private lateinit var menu: Menu

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		this.menu = menu
		val menuInflater = menuInflater
		menuInflater.inflate(R.menu.menu_main, menu)
		observeActions()
		return true
	}

	override fun getMenuRefreshObservable(): Observable<Any> {
		return RxMenuItem.clicks(menu.findItem(R.id.action_refresh))
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return when (item.itemId) {
			R.id.action_settings -> {
				val intent = Intent(this, SettingsActivity::class.java)
				startActivity(intent)
				true
			}
			R.id.action_add_exchange -> {
				val dialog = AddExchangeDialogFragment.newInstance(this)
				dialog.show(supportFragmentManager, "AddExchange")
				true
			}
			R.id.action_refresh -> {
				balancesPresenter.clearCache()
				balancesPresenter.loadBalances()
				true
			}
			else -> super.onOptionsItemSelected(item)
		}

	}

	override fun updateHeaderWithBalances(btcBalance: Double, usdBalance: Double) {
		runOnUiThread {
			Timber.d("updateHeaderWithBalances: $btcBalance $usdBalance")
			binding.lblTotalBtcBalance.text = String.format("\u20BF%.4f", btcBalance)
			binding.lblTotalUsdBalance.text = String.format("$%.2f", usdBalance)
		}
	}

	override fun updateExchangeData(updatedExchangeData: ExchangeExpandableGroup) {
		runOnUiThread {
			adapter?.updateExchangeData(updatedExchangeData)
		}
	}

	override fun updateBalanceData(updatedBalanceData: Balance) {
		adapter?.updateBalanceData(updatedBalanceData)
	}

	override fun onUnknownError(errorMessage: String) {

	}

	override fun onTimeout() {

	}

	override fun onNetworkError() {

	}

	override fun showProgress() {
		binding.progressBar.visibility = View.VISIBLE
	}

	override fun hideProgress() {
		binding.progressBar.visibility = View.GONE
	}

	override fun showExchangesLoadingProgress(exchangeNames: MutableSet<String>?) {
		val exchanges = ArrayList<ExchangeExpandableGroup>()
		exchangeNames?.forEach { exchangeName ->
			exchanges.add(ExchangeExpandableGroup(exchangeName, arrayListOf(Balance("empty", "", "", 0.0, 0.0))))
		}
		adapter = BalanceExpandableAdapter(exchanges, this@BalancesActivity)
		binding.recyclerBalance.adapter = adapter
	}

	override fun startChartActivity(exchange: String, symbol: String) {
		startActivity(ChartActivity.createIntent(this, exchange, symbol))
	}

	override fun scrollToPosition(exchange: String) {
		adapter?.getFlattenedGroupPosition(exchange)?.let { position -> layoutManager?.scrollToPositionWithOffset(position, 0) }
	}

	//region render

	private fun observeActions() {
		eventDisposables.clear()
		getMenuRefreshObservable()
				.take(1)
				?.subscribe {
					balancesPresenter.clearCache()
					balancesPresenter.loadBalances()
				}
				?.let { eventDisposables.add(it) }
	}

	//endregion
}