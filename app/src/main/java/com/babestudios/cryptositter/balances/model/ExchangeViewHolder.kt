package com.babestudios.cryptositter.balances.model

import android.view.View
import com.babestudios.base.rx.RxBus
import com.babestudios.base.views.expandablerecyclerview.viewholders.GroupViewHolder
import com.babestudios.cryptositter.R
import com.babestudios.cryptositter.databinding.ListItemBalancesExchangeBinding


class ExchangeViewHolder(itemView: View) : GroupViewHolder(itemView) {

	val binding = ListItemBalancesExchangeBinding.bind(itemView)

	init {
		binding.pgExchangeItem.visibility = View.VISIBLE
	}

	fun setBalancesItemExchange(exchangeExpandableGroup: ExchangeExpandableGroup) {
		binding.lblExchangeItemName.text = exchangeExpandableGroup.title
	}

	fun hideProgress() {
		binding.pgExchangeItem.visibility = View.GONE
		binding.ivExchangeItemArrow.visibility = View.VISIBLE
	}

	override fun expand() {
		RxBus.publish(RxBus.SUBJECT_BALANCES_EXCHANGE_CLICKED, binding.lblExchangeItemName.text)
		binding.ivExchangeItemArrow.setImageResource(R.drawable.ic_arrow_drop_up_black)
	}

	override fun collapse() {
		binding.ivExchangeItemArrow.setImageResource(R.drawable.ic_arrow_drop_down_black)
	}
}