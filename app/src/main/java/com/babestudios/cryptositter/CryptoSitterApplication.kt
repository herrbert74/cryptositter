package com.babestudios.cryptositter

import android.app.Activity
import android.app.Application
import android.content.Context
import com.babestudios.base.ext.DelegatesExt
//import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.paperdb.Paper
import timber.log.Timber
import timber.log.Timber.Forest.plant
import javax.inject.Inject


class CryptoSitterApplication : Application(), HasAndroidInjector {

	@Inject
	lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

	@Inject lateinit var androidInjector : DispatchingAndroidInjector<Any>

	override fun androidInjector(): AndroidInjector<Any> = androidInjector

	override fun onCreate() {
		super.onCreate()
		/*if (LeakCanary.isInAnalyzerProcess(this)) {
			// This process is dedicated to LeakCanary for heap analysis.
			// You should not init your app in this process.
			return
		}
		LeakCanary.install(this)*/
		instance = this
		Paper.init(this)
		if (BuildConfig.DEBUG) {
			plant(Timber.DebugTree())
		}
		DaggerAppComponent.builder()
				.application(this)
				.build()
				.inject(this)
	}

	companion object {
		var instance: CryptoSitterApplication by DelegatesExt.notNullSingleValue()
		val context: Context
			get() = instance.applicationContext
	}

	/*override fun androidInjector(): AndroidInjector<Any> {
		TODO("Not yet implemented")
	}*/
}
