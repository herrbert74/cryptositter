package com.babestudios.cryptositter.apikeys

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import android.util.Base64.DEFAULT
import com.babestudios.cryptositter.CryptoSitterApplication
import com.babestudios.cryptositter.apikeys.model.ApiKeyModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.nio.charset.Charset
import java.security.Key
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.GCMParameterSpec

class ApiKeyRepository {


	companion object {
		private const val AndroidKeyStore: String = "AndroidKeyStore"
		private const val AES_MODE: String = "AES/GCM/NoPadding"
		private const val KEY_ALIAS: String = "CryptoSitterKey2"
		private const val FIXED_IV: String = "AKSICFM3K3L9"
		private const val SHARED_PREFERENCES_API_KEY: String = "ApiKeyPreferences"
		private const val SHARED_PREFERENCES_KEY_API_KEYS: String = "ApiKeys"


		fun saveApiKey(context: Context, exchange: String?, apiKey: String, apiSecret: String) {
			var apiKeys = loadApiKeys(context)
			if (apiKeys == null) {
				apiKeys = HashMap()
			}
			exchange?.let { apiKeys.put(it, ApiKeyModel(apiKey, apiSecret)) }
			saveApiKeys(context, encryptData(Gson().toJson(apiKeys).toByteArray()))
		}

		fun deleteApiKey(context: Context, apiKey: String?) {
			val apiKeys = loadApiKeys(context)
			if (apiKeys != null) {
				apiKeys.remove(apiKey)
				saveApiKeys(context, encryptData(Gson().toJson(apiKeys).toByteArray()))
			}
		}

		fun loadApiKeyForExchange(exchangeAndOwner: String): ApiKeyModel? {
			return loadApiKeys(CryptoSitterApplication.context)?.get(exchangeAndOwner)
		}

		fun getExchangeNamesWithApiKeys(): MutableSet<String>? {
			return loadApiKeys(CryptoSitterApplication.context)?.keys
		}

		private fun getSecretKey(): Key? {
			val keyStore = KeyStore.getInstance(AndroidKeyStore)
			keyStore.load(null)

			return if (!keyStore.containsAlias(KEY_ALIAS)) {
				generateKey()
			} else {
				loadSecretKey()
			}
		}

		private fun generateKey(): SecretKey {
			val keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, AndroidKeyStore)
			keyGenerator.init(
					KeyGenParameterSpec.Builder(KEY_ALIAS, KeyProperties.PURPOSE_ENCRYPT.or(KeyProperties.PURPOSE_DECRYPT))
							.setBlockModes(KeyProperties.BLOCK_MODE_GCM).setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
							.setRandomizedEncryptionRequired(false)
							.build())
			return keyGenerator.generateKey()
		}

		@Throws(Exception::class)
		private fun loadSecretKey(): Key? {
			val keyStore = KeyStore.getInstance(AndroidKeyStore)
			keyStore.load(null)
			return keyStore.getKey(KEY_ALIAS, null)
		}

		private fun encryptData(input: ByteArray?): String {
			val c = Cipher.getInstance(AES_MODE)
			c.init(Cipher.ENCRYPT_MODE, getSecretKey(), GCMParameterSpec(128, FIXED_IV.toByteArray()))
			val encodedBytes = c.doFinal(input)
			return Base64.encodeToString(encodedBytes, DEFAULT)
		}

		private fun decryptData(encoded: ByteArray): ByteArray {
			val encrypted = Base64.decode(encoded, DEFAULT)
			val c = Cipher.getInstance(AES_MODE)
			c.init(Cipher.DECRYPT_MODE, getSecretKey(), GCMParameterSpec(128, FIXED_IV.toByteArray()))
			return c.doFinal(encrypted)
		}


		private fun saveApiKeys(context: Context, input: String) {
			val sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_API_KEY, MODE_PRIVATE)
			val editor = sharedPreferences.edit()
			editor.putString(SHARED_PREFERENCES_KEY_API_KEYS, input)
			editor.apply()
		}

		fun loadApiKeys(context: Context): HashMap<String, ApiKeyModel>? {
			val sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_API_KEY, MODE_PRIVATE)
			return if (sharedPreferences.contains(SHARED_PREFERENCES_KEY_API_KEYS)) {
				val t = sharedPreferences.getString(SHARED_PREFERENCES_KEY_API_KEYS, "") ?: ""
				Gson().fromJson(
						decryptData(t.toByteArray()).toString(Charset.defaultCharset()),
						object : TypeToken<HashMap<String, ApiKeyModel>>() {}.type
				)
			} else {
				null
			}
		}
	}
}