package com.babestudios.cryptositter

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, DataContractModule::class, ActivityBuilder::class])
interface AppComponent {

	@Component.Builder
	interface Builder {
		@BindsInstance
		fun application(app: Application): Builder

		fun build(): AppComponent
	}

	fun inject(app: CryptoSitterApplication)

	// no need to expose to sub-graphs the schedulerProvider, and apiService instances
}