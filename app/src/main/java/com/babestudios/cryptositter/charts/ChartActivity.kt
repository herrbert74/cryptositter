package com.babestudios.cryptositter.charts

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.babestudios.base.ext.viewBinding
import com.babestudios.cryptositter.R
import com.babestudios.cryptositter.databinding.ActivityChartBinding
import com.github.mikephil.charting.data.CandleData
import com.github.mikephil.charting.data.CandleDataSet
import dagger.android.AndroidInjection
import javax.inject.Inject


class ChartActivity : AppCompatActivity(), ChartView {

	private val binding by viewBinding(ActivityChartBinding::inflate)

	companion object {
		fun createIntent(context: Context, exchange: String, symbol: String): Intent {
			val intent = Intent(context, ChartActivity::class.java)
			intent.putExtra("exchange", exchange)
			intent.putExtra("symbol", symbol)
			return intent
		}
	}

	@Inject
	lateinit var chartPresenter: ChartPresenter

	override fun onCreate(savedInstanceState: Bundle?) {
		AndroidInjection.inject(this)
		super.onCreate(savedInstanceState)
		setContentView(binding.root)
		val exchange = intent.getStringExtra("exchange")
		val symbol = intent.getStringExtra("symbol")
		supportActionBar?.title = "$exchange : $symbol"
		chartPresenter.model = ChartScreenModel(exchange ?: "", symbol ?: "")
	}

	override fun onResume() {
		chartPresenter.bindView(this)
		super.onResume()
	}

	override fun onPause() {
		chartPresenter.unbindView()
		super.onPause()
	}

	override fun showChart(candleDataSet: CandleDataSet, timeStamps: ArrayList<Long>, exchange: String?) {
		val candleData = CandleData(candleDataSet)
		binding.chart.data = candleData
		binding.chart.setBackgroundColor(getColor(R.color.colorBackground))
		binding.chart.xAxis?.textColor = Color.WHITE
		binding.chart.axisLeft?.textColor = Color.WHITE
		binding.chart.axisRight?.textColor = Color.WHITE
		//binding.chart.setVisibleXRangeMaximum(50f)
		binding.chart.legend?.isEnabled = false
		//binding.chart.isScaleXEnabled = false
		binding.chart.moveViewToX(candleDataSet.entryCount - 50f)
		binding.chart.xAxis?.valueFormatter = DateAxisFormatter(timeStamps)
		binding.chart.invalidate()
	}

	override fun onUnknownError(errorMessage: String) {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun onTimeout() {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun onNetworkError() {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun showProgress() {
		binding.progressBar.visibility = View.VISIBLE
	}

	override fun hideProgress() {
		binding.progressBar.visibility = View.GONE
	}

}