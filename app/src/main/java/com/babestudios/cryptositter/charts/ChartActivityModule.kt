package com.babestudios.cryptositter.charts

import com.babestudios.base.rx.SchedulerProvider
import com.babestudios.cryptositter.data.network.binance.BinanceRepository
import com.babestudios.cryptositter.data.network.bitfinex.BitfinexRepository
import com.babestudios.cryptositter.data.network.bittrex.BittrexRepository
import dagger.Module
import dagger.Provides

@Module
class ChartActivityModule {

	@Provides
	fun providePresenter(binanceRepository: BinanceRepository,
						 bittrexRepository: BittrexRepository,
						 bitfinexRepository: BitfinexRepository,
						 schedulerProvider: SchedulerProvider) = ChartPresenter(binanceRepository, bittrexRepository, bitfinexRepository, schedulerProvider)
}