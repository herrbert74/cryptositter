package com.babestudios.cryptositter.charts


import com.babestudios.base.ext.toDateString
import com.babestudios.base.ext.toTimeString
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter

class DateAxisFormatter(private val timeStamps: ArrayList<Long>) : IAxisValueFormatter {

	override fun getFormattedValue(value: Float, axis: AxisBase): String {

		return if(timeStamps[value.toInt()].toTimeString() == "00:15" || timeStamps[value.toInt()].toTimeString() == "23:45") {
			timeStamps[value.toInt()].toDateString()
		} else {
			timeStamps[value.toInt()].toTimeString()
		}
	}
}