package com.babestudios.cryptositter.charts

import android.graphics.Color
import android.graphics.Paint
import android.util.Log
import com.babestudios.base.mvp.BaseRxPresenter
import com.babestudios.base.rx.SchedulerProvider
import com.babestudios.cryptositter.data.network.binance.BinanceRepository
import com.babestudios.cryptositter.data.network.bitfinex.BitfinexRepository
import com.babestudios.cryptositter.data.network.bittrex.BittrexRepository
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.CandleDataSet
import com.github.mikephil.charting.data.CandleEntry
import io.reactivex.subscribers.ResourceSubscriber


class ChartPresenter(private var binanceRepository: BinanceRepository,
					 private var bittrexRepository: BittrexRepository,
					 private var bitfinexRepository: BitfinexRepository,
					 private var schedulerProvider: SchedulerProvider) : BaseRxPresenter<ChartScreenModel, ChartView>() {

	override fun updateView() {
		loadChartData()
	}

	override fun bindView(view: ChartView) {
		super.bindView(view)
		if (disposables.size() != 0) {
			view()?.showProgress()
		}
	}

	private fun loadChartData() {
		model?.let { model ->
			view()?.showProgress()
			disposables.add(binanceRepository.getBinanceCandles(model.symbol, model.interval).subscribeWith(object : ResourceSubscriber<ArrayList<ArrayList<Any>>>() {
				override fun onNext(t: ArrayList<ArrayList<Any>>) {
					val entries = ArrayList<CandleEntry>()
					t.forEach {
						val candleEntry = CandleEntry(entries.size.toFloat(), it[2].toString().toFloat(), it[3].toString().toFloat(), it[1].toString().toFloat(), it[4].toString().toFloat(), it[0])
						entries.add(candleEntry)
					}
					val candleDataSet = CandleDataSet(entries, "")
					candleDataSet.setDrawIcons(false)
					candleDataSet.axisDependency = YAxis.AxisDependency.LEFT
					candleDataSet.shadowWidth = 0.7f
					candleDataSet.decreasingColor = Color.RED
					candleDataSet.decreasingPaintStyle = Paint.Style.FILL
					candleDataSet.increasingColor = Color.rgb(122, 242, 84)
					candleDataSet.increasingPaintStyle = Paint.Style.STROKE
					candleDataSet.neutralColor = Color.BLUE
					candleDataSet.setDrawValues(false)
					candleDataSet.highLightColor = Color.WHITE
					candleDataSet.shadowColorSameAsCandle = true
					view()?.showChart(candleDataSet, getTimeStampsArray(entries), model.exchange)
				}

				override fun onComplete() {
					disposables.clear()
					view()?.hideProgress()
				}

				override fun onError(e: Throwable) {
					Log.d("test", "onError: " + e.localizedMessage)
				}
			}))
		}
	}

	private fun getTimeStampsArray(entries: ArrayList<CandleEntry>): ArrayList<Long> {
		val timestamps = ArrayList<Long>()
		entries.forEach { timestamps.add((it.data as Double).toLong()) }
		return timestamps
	}
}

