package com.babestudios.cryptositter.charts

import com.babestudios.base.mvp.BaseView
import com.github.mikephil.charting.data.CandleDataSet

interface ChartView : BaseView {

	fun showChart(candleDataSet: CandleDataSet, timeStamps: ArrayList<Long>, exchange: String?)

}