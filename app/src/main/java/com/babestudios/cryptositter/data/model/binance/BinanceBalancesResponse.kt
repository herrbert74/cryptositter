package com.babestudios.cryptositter.data.model.binance

import com.babestudios.cryptositter.data.model.base.Response


@Suppress("unused")
class BinanceBalancesResponse(
		var balances: ArrayList<BinanceBalance>,
		var canTrade: Boolean,
		var canWithdraw: Boolean,
		var canDeposit: Boolean,
		var makerCommission: Int,
		var takerCommission: Int,
		var buyerCommission: Int,
		var sellerCommission: Int,
		override var updateTime: Long
) : Response
