package com.babestudios.cryptositter.data.model

enum class Exchange(var key: String) {
	BITTREX("Bittrex"), BINANCE("Binance"), BITFINEX("Bitfinex"), FTX("Ftx")
}
