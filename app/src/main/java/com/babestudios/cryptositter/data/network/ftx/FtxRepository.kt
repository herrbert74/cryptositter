package com.babestudios.cryptositter.data.network.ftx

import com.babestudios.cryptositter.BuildConfig
import com.babestudios.cryptositter.apikeys.ApiKeyRepository
import com.babestudios.cryptositter.data.ALGORITHM_HMAC_SHA256
import com.babestudios.cryptositter.data.getNonce
import com.babestudios.cryptositter.data.hmacDigest
import com.babestudios.cryptositter.data.model.LastPriceWithChange
import com.babestudios.cryptositter.data.model.ftx.FtxBalancesResponse
import com.babestudios.cryptositter.data.model.ftx.FtxCandlesResponse
import com.babestudios.cryptositter.data.model.ftx.FtxTickerResponse
import com.babestudios.cryptositter.data.network.base.CryptoRepository
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.rxkotlin.Flowables
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class FtxRepository @Inject constructor(private val ftxService: FtxService) : CryptoRepository {

	private fun getSignature(exchangeWithOwner: String, @Suppress("SameParameterValue") payload: String) =
			hmacDigest(
					payload,
					ApiKeyRepository.loadApiKeyForExchange(exchangeWithOwner)?.apiSecret,
					ALGORITHM_HMAC_SHA256
			)

	override fun getBitcoinTickerSymbol(symbol: String): String {
		return when (symbol) {
			"BTC" -> "BTC_USD"
			"USD" -> "BTC_USD"
			else -> "${symbol}_BTC"
		}
	}

	//region authenticated network calls

	fun getFtxBalancesResponse(exchangeWithOwner: String): Flowable<FtxBalancesResponse> {
		val timeStamp = getNonce().toString()
		val apiKey = ApiKeyRepository.loadApiKeyForExchange(exchangeWithOwner)?.apiKey ?: ""
		val path = "/api/${BuildConfig.FTX_BALANCES_ENDPOINT}"
		val payload = "${timeStamp}GET$path"
		val signature = getSignature(exchangeWithOwner, payload)
		return ftxService.getFtxBalances(apiKey, timeStamp, signature).subscribeOn(Schedulers.newThread())
	}

	//endregion

	//region unauthenticated network calls

	override fun ticker(symbol: String): Single<Double> {
		return ftxService.getFtxTicker(symbol).map { it.result.last }
	}

	override fun lastPriceWithChange(symbol: String): Single<LastPriceWithChange> {
		return Flowables.combineLatest(
				getFtxTicker(symbol),
				getFtxCandles(symbol,
						"3600",
						"24",
						getNonce().toString(),
						getNonce().toString()),
		) { ticker, candles ->
			val close24hAgo = candles.result[0].close
			LastPriceWithChange(
					ticker.result.last,
					ticker.result.last - close24hAgo,
					(ticker.result.last - close24hAgo) / close24hAgo * 100
			)
		}.firstOrError()
	}

	fun getFtxCandles(
			symbol: String,
			resolution: String,
			limit: String,
			startTime: String,
			endTime: String,
	): Flowable<FtxCandlesResponse> {
		return ftxService.getFtxCandles(symbol, resolution, limit/*, startTime, endTime*/).toFlowable()
	}

	fun getFtxTicker(symbol: String): Flowable<FtxTickerResponse> {
		return ftxService.getFtxTicker(symbol)
				.toFlowable()
	}

	//endregion
}