package com.babestudios.cryptositter.data.network.converters

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type
import javax.inject.Inject

class AdvancedGsonConverterFactory @Inject constructor(private var gson: Gson): Converter.Factory() {

	override fun responseBodyConverter(type: Type, annotations: Array<Annotation>, retrofit: Retrofit): Converter<ResponseBody, *> {
		val adapter = gson.getAdapter(TypeToken.get(type))
		return AdvancedGsonResponseBodyConverter(gson, adapter)
	}
}
