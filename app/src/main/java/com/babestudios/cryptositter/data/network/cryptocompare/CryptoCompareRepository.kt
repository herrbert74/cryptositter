package com.babestudios.cryptositter.data.network.cryptocompare

import com.babestudios.cryptositter.data.model.cryptocompare.CoinList
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CryptoCompareRepository @Inject constructor(private val cryptoCompareService: CryptoCompareService) {

	fun getCoinList(): Observable<CoinList> {
		return cryptoCompareService.coinList
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
	}
}