package com.babestudios.cryptositter.data

import com.babestudios.cryptositter.R

object CryptoMaps {
	val logoMap = mutableMapOf(
			"ant" to R.drawable.ic_crypto_neo,
			"ardr" to R.drawable.ic_crypto_ardor,
			"bat" to R.drawable.ic_crypto_bat,
			"btc" to R.drawable.ic_crypto_btc,
			"bcc" to R.drawable.ic_crypto_bch,
			"bch" to R.drawable.ic_crypto_bch,
			"dash" to R.drawable.ic_crypto_dash,
			"etc" to R.drawable.ic_crypto_ethereum_classic,
			"eth" to R.drawable.ic_crypto_ethereum,
			"game" to R.drawable.ic_crypto_gamecredits,
			"iot" to R.drawable.ic_crypto_iota,
			"kmd" to R.drawable.ic_crypto_komodo,
			"ltc" to R.drawable.ic_crypto_litecoin,
			"maid" to R.drawable.ic_crypto_maidsafecoin,
			"mco" to R.drawable.ic_crypto_monaco,
			"omg" to R.drawable.ic_crypto_omg,
			"qrk" to R.drawable.ic_crypto_quark,
			"xrp" to R.drawable.ic_crypto_ripple,
			"usd" to R.drawable.ic_crypto_usd,
			"usdt" to R.drawable.ic_crypto_usd_tether,
			"zec" to R.drawable.ic_crypto_zcash)
			.withDefault { R.drawable.ic_crypto_unknown }

	val currencyNameMap = mutableMapOf(
			"ant" to "NEO",
			"ardr" to "Ardor",
			"bat" to "Basic Attention Token",
			"btc" to "Bitcoin",
			"btg" to "Bitcoin Gold",
			"bcc" to "BCash",
			"bch" to "BCash",
			"dash" to "Dash",
			"etc" to "Ethereum Classic",
			"eth" to "Ethereum",
			"game" to "GameCredits",
			"iot" to "IOTA",
			"kmd" to "Komodo",
			"ltc" to "LiteCoin",
			"maid" to "MaidSafeCoin",
			"mco" to "Monaco",
			"omg" to "OmiseGO",
			"qrk" to "Quark",
			"xrp" to "Ripple",
			"usd" to "US Dollar",
			"usdt" to "USD Tether",
			"zec" to "zcash")
			.withDefault { "" }
}