package com.babestudios.cryptositter.data.model.bittrex

import com.babestudios.cryptositter.data.model.base.Response


class BittrexGetTickerResponse(
		val bidRate: Double,
		val askRate: Double,
		val lastTradeRate: Double
) : Response {
	override var updateTime: Long = 0
}
