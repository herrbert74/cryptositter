package com.babestudios.cryptositter.data.model.bitfinex

import com.babestudios.cryptositter.data.model.base.Response

data class BitfinexAccountInfosResponse(var id: String) : Response {
	override var updateTime: Long = 0
}
