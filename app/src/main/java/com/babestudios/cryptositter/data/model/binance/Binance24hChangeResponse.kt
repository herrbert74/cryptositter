package com.babestudios.cryptositter.data.model.binance

import com.babestudios.cryptositter.data.model.base.Response


data class Binance24hChangeResponse(
		val priceChange: Double,
		val priceChangePercent: Double,
		val weightedAvgPrice: Double,
		val prevClosePrice: Double,
		val lastPrice: Double,
		val bidPrice: Double,
		val askPrice: Double,
		val openPrice: Double,
		val highPrice: Double,
		val lowPrice: Double,
		val volume: Double,
		val openTime: Long,
		val closeTime: Long,
		val firstId: Int,
		val lastId: Int,
		val count: Int
) : Response {
	override var updateTime: Long = 0
}