package com.babestudios.cryptositter.data.model.ftx

import com.babestudios.cryptositter.domain.model.Balance


data class FtxBalance(
		var coin: String,
		private var total: String,
		var free: String
) {

	fun convertToBalance(): Balance {
		return Balance("Ftx", coin, coin, total.toDouble(), lastBtcUsdPrice)
	}

	var lastBtcUsdPrice: Double = 0.0
}
