package com.babestudios.cryptositter.data.model.bitfinex

import com.babestudios.cryptositter.domain.model.Balance


data class BitfinexBalance(var type: String, var currency: String, private var amount: String, var available: String) {
	fun convertToBalance(): Balance {
		return Balance("Bitfinex", currency, "", amount.toDouble(), lastBtcUsdPrice)
	}

	var lastBtcUsdPrice: Double = 0.0
}
