package com.babestudios.cryptositter.data.model.base


interface Response {
	var updateTime: Long
}