package com.babestudios.cryptositter.data.model.bittrex

import com.babestudios.cryptositter.domain.model.Balance


class BittrexBalance(
		val currencySymbol: String,
		val total: Double,
		val available: Double,
		val updatedAt: String
) {

	var lastBtcUsdPrice: Double = 0.0

	fun convertToBalance(): Balance {
		return Balance("Bittrex", currencySymbol, "", total, lastBtcUsdPrice)
	}
}