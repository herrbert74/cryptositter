package com.babestudios.cryptositter.data.network.ftx

import com.babestudios.cryptositter.BuildConfig
import com.babestudios.cryptositter.data.model.ftx.FtxBalancesResponse
import com.babestudios.cryptositter.data.model.ftx.FtxCandlesResponse
import com.babestudios.cryptositter.data.model.ftx.FtxTickerResponse
import io.reactivex.Flowable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface FtxService {

	@GET(BuildConfig.FTX_BALANCES_ENDPOINT)
	fun getFtxBalances(
			@Header("FTX-KEY") apiKey: String,
			@Header("FTX-TS") timeStamp: String,
			@Header("FTX-SIGN") signature: String,
	): Flowable<FtxBalancesResponse>

	@GET(BuildConfig.FTX_GET_TICKER_ENDPOINT)
	fun getFtxTicker(@Path("symbol") symbol: String): Single<FtxTickerResponse>

	@GET(BuildConfig.FTX_CANDLES_ENDPOINT)
	fun getFtxCandles(
			@Path("symbol") symbol: String,
			@Query("resolution") resolution: String,
			@Query("limit") limit: String,
			/*@Query("start_time") startTime: String,
			@Query("end_time") endTime: String,*/
	): Single<FtxCandlesResponse>
}


