package com.babestudios.cryptositter.data.model.bitfinex


import com.babestudios.cryptositter.data.model.base.Response

data class BitfinexTickerResponse(var ticker: ArrayList<Double>) : Response {
	override var updateTime: Long = 0
}
