package com.babestudios.cryptositter.data.model.bittrex

import com.babestudios.cryptositter.data.model.base.Response


abstract class BittrexResponse<M>(
		@Transient open var result: M,
		@Transient open var success: Boolean,
		@Transient open var message: String
) : Response {
	override var updateTime: Long = 0
}