package com.babestudios.cryptositter.data.model.ftx


data class FtxTickerResponse(
		override var result: FtxTickerResult,
		override var success: Boolean,
) : FtxResponse<FtxTickerResult>(result, success)
