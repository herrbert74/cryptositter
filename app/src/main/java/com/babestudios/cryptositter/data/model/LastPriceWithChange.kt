package com.babestudios.cryptositter.data.model

data class LastPriceWithChange(
		val lastPrice: Double = 0.0,
		val dailyChange: Double = 0.0,
		val dailyChangePercentage: Double = 0.0
)
