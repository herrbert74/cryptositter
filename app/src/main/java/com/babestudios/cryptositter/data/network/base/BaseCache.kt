package com.babestudios.cryptositter.data.network.base

import com.babestudios.cryptositter.data.model.base.Response
import io.paperdb.Paper
import io.reactivex.Maybe
import java.util.*

/**
 * Cache age upper limit is the maximum age, where the cache will be used.
 * Cache age lower limit is the maximum age, where the cache won't be updated in the background.
 */
val KEY_CACHE_AGE_UPPER_LIMIT = Date(System.currentTimeMillis() - (12 * 60 * 60 * 1000)) //12 hours
val KEY_CACHE_AGE_LOWER_LIMIT = Date(System.currentTimeMillis() - (60 * 60 * 1000)) //1 hour

enum class REQUEST(var key: String) {
	GET_BALANCES("getBalances"), GET_24_H_CHANGE("get24HChange"), GET_TICKER("getTicker")
}

abstract class BaseCache {

	abstract val exchange: String

	open fun <R : Response> getResponse(request: String, ticker: String?): Maybe<R> {
		val normalizedTicker = ticker?.replace('/', '_')
		var response: R? = null
		try {
			response = Paper.book().read<R>("${exchange}_$request" + if (normalizedTicker.isNullOrBlank()) "" else "_$normalizedTicker")
		} catch (ignored: Exception) {
		}
		return if (response == null || Date(response.updateTime).before(KEY_CACHE_AGE_UPPER_LIMIT)) {
			Maybe.empty()
		} else {
			Maybe.just(response)
		}
	}

	open fun <R : Response> putResponse(request: String, response: R, ticker: String?) {
		val normalizedTicker = ticker?.replace('/', '_')
		response.updateTime = System.currentTimeMillis()
		Paper.book().write<R>("${exchange}_$request" + if (normalizedTicker.isNullOrBlank()) "" else "_$normalizedTicker", response)
	}

	fun clearResponses() {
		Paper.book().allKeys.listIterator().forEach {
			if (it.startsWith(exchange)) {
				Paper.book().delete(it)
			}
		}
	}

}