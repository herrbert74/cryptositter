package com.babestudios.cryptositter.data.model.bittrex

import com.google.gson.annotations.SerializedName

data class BittrexMarketSummary(
		val symbol: String, //BTC-USDT or LTC-BTC
		val high: Double, //0.01350000
		val low: Double, //0.01200000
		val volume: Double, //3833.97619253
		val quiteVolume: Double, //3833.97619253
		val percentChange: Double, //0.27
		val updatedAt: String, //2014-02-13T00:00:00
)