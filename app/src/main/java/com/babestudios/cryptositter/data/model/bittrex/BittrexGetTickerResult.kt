package com.babestudios.cryptositter.data.model.bittrex

data class BittrexGetTickerResult(
		val bidRate : Double,
		val askRate : Double,
		val lastTradeRate : Double
)