package com.babestudios.cryptositter.data.model.bittrex

import com.babestudios.cryptositter.data.model.base.Response


data class BittrexBalancesResponse(
		val balances: List<BittrexBalance>
) : Response {
	override var updateTime: Long = 0
}