package com.babestudios.cryptositter.data.network.binance

import com.babestudios.cryptositter.apikeys.ApiKeyRepository
import com.babestudios.cryptositter.data.ALGORITHM_HMAC_SHA256
import com.babestudios.cryptositter.data.getNonce
import com.babestudios.cryptositter.data.hmacDigest
import com.babestudios.cryptositter.data.model.LastPriceWithChange
import com.babestudios.cryptositter.data.model.binance.Binance24hChangeResponse
import com.babestudios.cryptositter.data.model.binance.BinanceBalancesResponse
import com.babestudios.cryptositter.data.model.binance.BinanceGetTickerResponse
import com.babestudios.cryptositter.data.network.base.CryptoRepository
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.Flowables
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BinanceRepository @Inject constructor(private val binanceService: BinanceService) : CryptoRepository {

	private val recvWindow = 59999L
	private val excludedTickerSet = setOf("VEN")

	private fun getSignature(exchangeWithOwner: String, timeStamp: Long): String {
		return hmacDigest(
				"recvWindow=$recvWindow&timestamp=$timeStamp",
				ApiKeyRepository.loadApiKeyForExchange(exchangeWithOwner)?.apiSecret,
				ALGORITHM_HMAC_SHA256
		)
	}

	override fun getBitcoinTickerSymbol(symbol: String): String {
		return when (symbol) {
			"BTC" -> "BTCUSDT"
			"USDT" -> "BTCUSDT"
			else -> "${symbol}BTC"
		}
	}

	//region authenticated network calls

	fun getBinanceBalancesResponse(exchangeWithOwner: String): Flowable<BinanceBalancesResponse> {
		val timeStamp = getNonce()
		val signature = getSignature(exchangeWithOwner, timeStamp)
		return binanceService.getBinanceBalances(
				ApiKeyRepository.loadApiKeyForExchange(exchangeWithOwner)?.apiKey ?: "",
				recvWindow,
				timeStamp,
				signature
		)
	}

	//endregion

	//region unauthenticated network calls

	override fun ticker(symbol: String): Single<Double> {
		return binanceService.getBinanceTicker(symbol).map { it.price }.firstOrError()
	}

	override fun lastPriceWithChange(symbol: String): Single<LastPriceWithChange> {
		return Flowables.combineLatest(
				getBinanceTicker(symbol),
				getBinance24hChange(symbol)
		) { ticker, change ->
			LastPriceWithChange(
					ticker.price,
					change.priceChange,
					change.priceChangePercent
			)
		}.firstOrError()
	}

	fun getBinanceTicker(symbol: String): Flowable<BinanceGetTickerResponse> {
		return binanceService.getBinanceTicker(symbol)
	}

	fun getBinance24hChange(symbol: String): Flowable<Binance24hChangeResponse> {
		return binanceService.getBinance24hChange(symbol)
	}

	fun getBinanceCandles(symbol: String, interval: String): Flowable<ArrayList<ArrayList<Any>>> {
		return binanceService.getBinanceCandles(getBitcoinTickerSymbol(symbol), interval)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
	}

	//endregion

	private fun filterExclusionsFromBalances(balancesResponse: BinanceBalancesResponse): BinanceBalancesResponse {
		val list = balancesResponse.balances
		balancesResponse.balances = ArrayList(list.filter { binanceBalance -> !excludedTickerSet.contains(binanceBalance.asset) })
		return balancesResponse
	}
}
