package com.babestudios.cryptositter.data.network.bittrex

import com.babestudios.cryptositter.data.model.Exchange
import com.babestudios.cryptositter.data.model.bittrex.BittrexBalancesResponse
import com.babestudios.cryptositter.data.model.bittrex.BittrexGetTickerResponse
import com.babestudios.cryptositter.data.model.bittrex.BittrexMarketSummaryResponse
import com.babestudios.cryptositter.data.network.base.BaseCache
import com.babestudios.cryptositter.data.network.base.REQUEST
import io.reactivex.Maybe
import javax.inject.Inject


class BittrexCache @Inject constructor(): BaseCache() {

	override val exchange: String
		get() = Exchange.BITTREX.key

	fun getBalancesResponse(): Maybe<BittrexBalancesResponse> {
		return super.getResponse(REQUEST.GET_BALANCES.key, null)
	}

	fun putBalancesResponse(response: BittrexBalancesResponse) {
		super.putResponse(REQUEST.GET_BALANCES.key, response, null)
	}

	fun getTickerResponse(ticker: String): Maybe<BittrexGetTickerResponse> {
		return super.getResponse(REQUEST.GET_TICKER.key, ticker)
	}

	fun putTickerResponse(response: BittrexGetTickerResponse, ticker: String) {
		super.putResponse(REQUEST.GET_TICKER.key, response, ticker)
	}

	fun get24hResponse(ticker: String): Maybe<BittrexMarketSummaryResponse> {
		return super.getResponse(REQUEST.GET_24_H_CHANGE.key, ticker)
	}

	fun put24hResponse(response: BittrexMarketSummaryResponse, ticker: String) {
		super.putResponse(REQUEST.GET_24_H_CHANGE.key, response, ticker)
	}
}