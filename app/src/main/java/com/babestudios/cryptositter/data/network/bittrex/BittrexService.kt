package com.babestudios.cryptositter.data.network.bittrex

import com.babestudios.cryptositter.BuildConfig
import com.babestudios.cryptositter.data.model.bittrex.BittrexBalance
import com.babestudios.cryptositter.data.model.bittrex.BittrexGetTickerResponse
import com.babestudios.cryptositter.data.model.bittrex.BittrexMarketSummaryResponse
import io.reactivex.Flowable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface BittrexService {

	@GET(BuildConfig.BITTREX_BALANCES_ENDPOINT)
	fun getBittrexBalances(
			@Header("Api-Signature") signature: String,
			@Header("Api-Key") apiKey: String,
			@Header("Api-Content-Hash") contentHash: String,
			@Header("Api-Timestamp") timestamp: String
	): Flowable<List<BittrexBalance>>

	@GET(BuildConfig.BITTREX_GET_TICKER_ENDPOINT)
	fun getBittrexTicker(@Path("symbol") symbol: String): Single<BittrexGetTickerResponse>

	@GET(BuildConfig.BITTREX_24H_CHANGE_ENDPOINT)
	fun getBittrexMarketSummary(@Path("symbol") symbol: String): Single<BittrexMarketSummaryResponse>
}


