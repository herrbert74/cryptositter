package com.babestudios.cryptositter.data.network.base

import com.babestudios.cryptositter.data.model.LastPriceWithChange
import io.reactivex.Single

interface CryptoRepository {

	fun getBitcoinTickerSymbol(symbol: String): String
	fun ticker(symbol: String): Single<Double>
	fun lastPriceWithChange(symbol: String): Single<LastPriceWithChange>
}
