package com.babestudios.cryptositter.data.network.binance

import com.babestudios.cryptositter.BuildConfig
import com.babestudios.cryptositter.data.model.binance.Binance24hChangeResponse
import com.babestudios.cryptositter.data.model.binance.BinanceBalancesResponse
import com.babestudios.cryptositter.data.model.binance.BinanceGetTickerResponse
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
import java.util.*

/**
 * https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md
 */

interface BinanceService {

	@GET(BuildConfig.BINANCE_BALANCES_ENDPOINT)
	fun getBinanceBalances(@Header("X-MBX-APIKEY") apiKey: String,
						   @Query("recvWindow") recvWindow: Long,
						   @Query("timestamp") timeStamp: Long,
						   @Query("signature") signature: String
	): Flowable<BinanceBalancesResponse>

	@GET(BuildConfig.BINANCE_GET_TICKER_ENDPOINT)
	fun getBinanceTicker(@Query("symbol") symbol: String): Flowable<BinanceGetTickerResponse>

	@GET(BuildConfig.BINANCE_24H_CHANGE_ENDPOINT)
	fun getBinance24hChange(@Query("symbol") symbol: String): Flowable<Binance24hChangeResponse>

	@GET(BuildConfig.BINANCE_CANDLES_ENDPOINT)
	fun getBinanceCandles(@Query("symbol") symbol: String, @Query("interval") interval: String?): Flowable<ArrayList<ArrayList<Any>>>
}


