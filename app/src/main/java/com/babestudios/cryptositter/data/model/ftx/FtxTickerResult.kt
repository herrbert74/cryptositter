package com.babestudios.cryptositter.data.model.ftx


class FtxTickerResult(
		val name: String,
		val baseCurrency: String,
		val quoteCurrency: String,
		val type: String,
		val underlying: String?, //future markets only
		val enabled: Boolean,
		val bid: Double,
		val ask: Double,
		val last: Double,
		val postOnly: Boolean,
		val priceIncrement: Double,
		val sizeIncrement: Double,
		val restricted: Boolean,
)