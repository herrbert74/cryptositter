package com.babestudios.cryptositter.data.model.bitfinex

import com.babestudios.cryptositter.data.model.base.Response


class BitfinexSymbolsResponse : Response {
	override var updateTime: Long = 0
}
