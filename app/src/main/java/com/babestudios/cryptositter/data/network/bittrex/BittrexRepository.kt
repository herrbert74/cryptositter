package com.babestudios.cryptositter.data.network.bittrex

import com.babestudios.cryptositter.BuildConfig
import com.babestudios.cryptositter.apikeys.ApiKeyRepository
import com.babestudios.cryptositter.data.ALGORITHM_HMAC_SHA512
import com.babestudios.cryptositter.data.generateHashSha512
import com.babestudios.cryptositter.data.getNonce
import com.babestudios.cryptositter.data.hmacDigest
import com.babestudios.cryptositter.data.model.LastPriceWithChange
import com.babestudios.cryptositter.data.model.bittrex.BittrexBalance
import com.babestudios.cryptositter.data.model.bittrex.BittrexGetTickerResponse
import com.babestudios.cryptositter.data.model.bittrex.BittrexMarketSummaryResponse
import com.babestudios.cryptositter.data.network.base.CryptoRepository
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.rxkotlin.Flowables
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class BittrexRepository @Inject constructor(private val bittrexService: BittrexService) : CryptoRepository {

	private fun getSignature(
			exchangeWithOwner: String,
			timeStamp: String,
			contentHash: String
	): String {
		return hmacDigest(
				"${timeStamp}${BuildConfig.BITTREX_BASE_URL}${BuildConfig.BITTREX_BALANCES_ENDPOINT}" +
						"GET${contentHash}",
				ApiKeyRepository.loadApiKeyForExchange(exchangeWithOwner)?.apiSecret,
				ALGORITHM_HMAC_SHA512
		)
	}

	override fun getBitcoinTickerSymbol(symbol: String): String {
		return when (symbol) {
			"BTC" -> "BTC-USDT"
			"USDT" -> "BTC-USDT"
			else -> "$symbol-BTC"
		}
	}

	//region authenticated network calls

	fun getBittrexBalancesResponse(exchangeWithOwner: String): Flowable<List<BittrexBalance>> {
		val timeStamp = getNonce().toString()
		val apiKey = ApiKeyRepository.loadApiKeyForExchange(exchangeWithOwner)?.apiKey
		val contentHash = generateHashSha512("")
		val signature = getSignature(exchangeWithOwner, timeStamp, contentHash)
		return bittrexService.getBittrexBalances(
				signature,
				apiKey ?: "",
				contentHash,
				timeStamp
		)
				.subscribeOn(Schedulers.newThread())
	}

	//endregion

	//region unauthenticated network calls

	override fun ticker(symbol: String): Single<Double> {
		return bittrexService.getBittrexTicker(symbol).map { it.lastTradeRate }
	}

	override fun lastPriceWithChange(symbol: String): Single<LastPriceWithChange> {
		return Flowables.combineLatest(
				getBittrexTicker(symbol),
				getBittrexMarketSummary(symbol)
		) { ticker, marketSummary ->
			val prevDayValue = ticker.lastTradeRate - (ticker.lastTradeRate * marketSummary.percentChange)
			LastPriceWithChange(
					ticker.lastTradeRate,
					ticker.lastTradeRate - prevDayValue,
					marketSummary.percentChange
			)
		}.firstOrError()
	}

	fun getBittrexMarketSummary(symbol: String): Flowable<BittrexMarketSummaryResponse> {
		return bittrexService.getBittrexMarketSummary(symbol).toFlowable()
	}

	fun getBittrexTicker(symbol: String): Flowable<BittrexGetTickerResponse> {
		return bittrexService.getBittrexTicker(symbol).toFlowable()
	}

	//endregion
}
