package com.babestudios.cryptositter.data.network.bitfinex

import com.babestudios.cryptositter.BuildConfig
import com.babestudios.cryptositter.data.model.bitfinex.BitfinexAccountInfosResponse
import com.babestudios.cryptositter.data.model.bitfinex.BitfinexSymbolsResponse
import io.reactivex.Flowable
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import java.util.*

interface BitfinexService {

	@GET(BuildConfig.BITFINEX_SYMBOLS_ENDPOINT)
	fun bitfinexSymbols(): Observable<BitfinexSymbolsResponse>

	@GET(BuildConfig.BITFINEX_TICKER_ENDPOINT)
	fun getBitfinexTicker(@Path("symbol") symbol: String): Flowable<ArrayList<Double>>

	@POST(BuildConfig.BITFINEX_ORDER_HISTORY_ENDPOINT)
	fun getBitfinexOrderHistory(
			@Header("X-BFX-APIKEY") apiKey: String,
			@Header("X-BFX-PAYLOAD") payLoad: String,
			@Header("X-BFX-SIGNATURE") signature: String
	): Flowable<BitfinexAccountInfosResponse>

	@POST(BuildConfig.BITFINEX_BALANCES_ENDPOINT)
	fun getBitfinexBalances(
			@Header("bfx-payload") payload: String,
			@Header("bfx-apikey") apiKey: String,
			@Header("bfx-nonce") timeStamp: String,
			@Header("bfx-signature") signature: String,
	): Flowable<ArrayList<ArrayList<Any>>>
}


