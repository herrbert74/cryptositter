package com.babestudios.cryptositter.data.network.ftx

import com.babestudios.cryptositter.data.model.Exchange
import com.babestudios.cryptositter.data.model.ftx.FtxBalancesResponse
import com.babestudios.cryptositter.data.model.ftx.FtxTickerResponse
import com.babestudios.cryptositter.data.model.ftx.FtxCandlesResponse
import com.babestudios.cryptositter.data.network.base.BaseCache
import com.babestudios.cryptositter.data.network.base.REQUEST
import io.reactivex.Maybe
import javax.inject.Inject


class FtxCache @Inject constructor(): BaseCache() {

	override val exchange: String
		get() = Exchange.FTX.key

	fun getBalancesResponse(exchangeWithOwner: String): Maybe<FtxBalancesResponse> {
		return super.getResponse(exchangeWithOwner.plus(REQUEST.GET_BALANCES.key), null)
	}

	fun putBalancesResponse(exchangeWithOwner: String, response: FtxBalancesResponse) {
		super.putResponse(exchangeWithOwner.plus(REQUEST.GET_BALANCES.key), response, null)
	}

	fun getTickerResponse(ticker: String): Maybe<FtxTickerResponse> {
		return super.getResponse(REQUEST.GET_TICKER.key, ticker)
	}

	fun putTickerResponse(response: FtxTickerResponse, ticker: String) {
		super.putResponse(REQUEST.GET_TICKER.key, response, ticker)
	}

	fun get24hResponse(ticker: String): Maybe<FtxCandlesResponse> {
		return super.getResponse(REQUEST.GET_24_H_CHANGE.key, ticker)
	}

	fun put24hResponse(response: FtxCandlesResponse, ticker: String) {
		super.putResponse(REQUEST.GET_24_H_CHANGE.key, response, ticker)
	}
}