package com.babestudios.cryptositter.data.model.binance

import com.babestudios.cryptositter.domain.model.Balance


class BinanceBalance(val asset: String, val free: Double, val locked: Double) {

	var lastBtcUsdPrice: Double = 0.0

	fun convertToBalance(): Balance {
		return Balance("Binance", asset, "", free + locked, lastBtcUsdPrice)
	}
}