package com.babestudios.cryptositter.data.network.bitfinex

import com.babestudios.cryptositter.apikeys.ApiKeyRepository
import com.babestudios.cryptositter.data.ALGORITHM_HMAC_SHA384
import com.babestudios.cryptositter.data.getNonce
import com.babestudios.cryptositter.data.hmacDigest
import com.babestudios.cryptositter.data.model.LastPriceWithChange
import com.babestudios.cryptositter.data.model.bitfinex.BitfinexBalance
import com.babestudios.cryptositter.data.network.base.CryptoRepository
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class BitfinexRepository @Inject constructor(private val bitfinexService: BitfinexService) : CryptoRepository {

	private fun getSignature(exchangeWithOwner: String, @Suppress("SameParameterValue") payload: String) =
			hmacDigest(
					payload,
					ApiKeyRepository.loadApiKeyForExchange(exchangeWithOwner)?.apiSecret,
					ALGORITHM_HMAC_SHA384
			)

	override fun getBitcoinTickerSymbol(symbol: String): String {
		return when (symbol) {
			"BTC" -> "tBTCUSD"
			"USD" -> "tBTCUSD"
			else -> "t${symbol}BTC"
		}
	}

	//region authenticated network calls

	@Suppress("SENSELESS_COMPARISON") //it[4] can be null!!
	private fun createBalanceFromArrayList(it: java.util.ArrayList<Any>) =
			BitfinexBalance(
					type = it[0].toString(),
					currency = it[1].toString(),
					amount = it[2].toString(),
					available = if (it[4] != null) it[4].toString() else "0.0"
			)

	fun getBitfinexBalancesResponse(exchangeWithOwner: String): Flowable<ArrayList<ArrayList<Any>>> {
		val timeStamp = getNonce().toString()
		val apiKey = ApiKeyRepository.loadApiKeyForExchange(exchangeWithOwner)?.apiKey ?: ""
		val payload = "/api/v2/auth/r/wallets${timeStamp}"
		val signature = getSignature(exchangeWithOwner, payload)
		return bitfinexService.getBitfinexBalances(
				payload,
				apiKey,
				timeStamp,
				signature
		)
				.subscribeOn(Schedulers.newThread())
	}

	//endregion

	//region unauthenticated network calls

	override fun ticker(symbol: String): Single<Double> {
		return getBitfinexTicker(symbol).map { it[6] }.firstOrError()
	}

	override fun lastPriceWithChange(symbol: String): Single<LastPriceWithChange> {
		return getBitfinexTicker(symbol).map {
			LastPriceWithChange(
					it[4],
					it[5],
					it[6]
			)
		}.firstOrError()
	}

	/**
	 * Also contains the 24 hour change data
	 *
	 * [
	 * BID,
	 * BID_SIZE,
	 * ASK,
	 * ASK_SIZE,
	 * DAILY_CHANGE,
	 * DAILY_CHANGE_PERC,
	 * LAST_PRICE,
	 * VOLUME,
	 * HIGH,
	 * LOW
	]
	 */
	fun getBitfinexTicker(symbol: String): Flowable<ArrayList<Double>> {
		return bitfinexService.getBitfinexTicker(symbol)
	}

	//endregion

}
