package com.babestudios.cryptositter.data.network.binance

import com.babestudios.cryptositter.data.model.Exchange
import com.babestudios.cryptositter.data.model.binance.Binance24hChangeResponse
import com.babestudios.cryptositter.data.model.binance.BinanceBalancesResponse
import com.babestudios.cryptositter.data.model.binance.BinanceGetTickerResponse
import com.babestudios.cryptositter.data.network.base.BaseCache
import com.babestudios.cryptositter.data.network.base.REQUEST
import io.reactivex.Maybe
import javax.inject.Inject


class BinanceCache @Inject constructor(): BaseCache() {

	override val exchange: String
	get() = Exchange.BINANCE.key

	fun getBalancesResponse(): Maybe<BinanceBalancesResponse> {
		return super.getResponse(REQUEST.GET_BALANCES.key, null)
	}

	fun putBalancesResponse(response: BinanceBalancesResponse) {
		super.putResponse(REQUEST.GET_BALANCES.key, response, null)
	}

	fun getTickerResponse(ticker: String): Maybe<BinanceGetTickerResponse> {
		return super.getResponse(REQUEST.GET_TICKER.key, ticker)
	}

	fun putTickerResponse(response: BinanceGetTickerResponse, ticker: String) {
		super.putResponse(REQUEST.GET_TICKER.key, response, ticker)
	}

	fun get24hChangeResponse(ticker: String): Maybe<Binance24hChangeResponse> {
		return super.getResponse(REQUEST.GET_24_H_CHANGE.key, ticker)
	}

	fun put24hResponse(response: Binance24hChangeResponse, ticker: String) {
		super.putResponse(REQUEST.GET_24_H_CHANGE.key, response, ticker)
	}
}