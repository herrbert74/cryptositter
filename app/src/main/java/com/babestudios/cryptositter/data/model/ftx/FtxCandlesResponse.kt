package com.babestudios.cryptositter.data.model.ftx

data class FtxCandlesResponse(
		override var result: List<FtxCandle>,
		override var success: Boolean,
) : FtxResponse<List<FtxCandle>>(result, success)
