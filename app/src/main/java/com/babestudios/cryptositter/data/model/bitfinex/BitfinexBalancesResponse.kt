package com.babestudios.cryptositter.data.model.bitfinex

import com.babestudios.cryptositter.data.model.base.Response

data class BitfinexBalancesResponse(var balances: ArrayList<ArrayList<Any>>) : Response {
	override var updateTime: Long = 0
}