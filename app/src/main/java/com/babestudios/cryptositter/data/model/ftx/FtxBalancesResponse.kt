package com.babestudios.cryptositter.data.model.ftx

data class FtxBalancesResponse(
		override var result: List<FtxBalance>,
		override var success: Boolean,
) : FtxResponse<List<FtxBalance>>(result, success)
