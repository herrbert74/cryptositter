package com.babestudios.cryptositter.data.model.binance

import com.babestudios.cryptositter.data.model.base.Response

data class BinanceGetTickerResponse(
		var price: Double = 0.toDouble(),
		var symbol: String = ""
) : Response {
	override var updateTime: Long = 0
}