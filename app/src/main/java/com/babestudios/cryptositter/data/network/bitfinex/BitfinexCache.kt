package com.babestudios.cryptositter.data.network.bitfinex

import com.babestudios.cryptositter.data.model.Exchange
import com.babestudios.cryptositter.data.model.bitfinex.BitfinexBalancesResponse
import com.babestudios.cryptositter.data.model.bitfinex.BitfinexTickerResponse
import com.babestudios.cryptositter.data.network.base.BaseCache
import com.babestudios.cryptositter.data.network.base.REQUEST
import io.reactivex.Maybe
import javax.inject.Inject


class BitfinexCache  @Inject constructor(): BaseCache() {

	override val exchange: String
		get() = Exchange.BITFINEX.key

	fun getBalancesResponse(): Maybe<BitfinexBalancesResponse> {
		return super.getResponse(REQUEST.GET_BALANCES.key, null)
	}

	fun putBalancesResponse(response: BitfinexBalancesResponse) {
		super.putResponse(REQUEST.GET_BALANCES.key, response, null)
	}

	fun getTickerResponse(ticker: String): Maybe<BitfinexTickerResponse> {
		return super.getResponse(REQUEST.GET_TICKER.key, ticker)
	}

	fun putTickerResponse(response: BitfinexTickerResponse, ticker: String) {
		super.putResponse(REQUEST.GET_TICKER.key, response, ticker)
	}
}