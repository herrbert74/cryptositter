package com.babestudios.cryptositter.data.model.ftx

import com.babestudios.cryptositter.data.model.base.Response


abstract class FtxResponse<M>(
		@Transient open var result: M,
		@Transient open var success: Boolean,
) : Response {
	override var updateTime: Long = 0
}