package com.babestudios.cryptositter.data.network.cryptocompare

import com.babestudios.cryptositter.BuildConfig
import com.babestudios.cryptositter.data.model.cryptocompare.CoinList

import io.reactivex.Observable
import retrofit2.http.GET

interface CryptoCompareService {

	@get:GET(BuildConfig.CRYPTOCOMPARE_COINLIST_ENDPOINT)
	val coinList: Observable<CoinList>
}


