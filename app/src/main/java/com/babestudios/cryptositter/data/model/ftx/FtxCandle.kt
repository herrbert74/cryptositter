package com.babestudios.cryptositter.data.model.ftx


class FtxCandle(
		val close: Double,
		val high: Double,
		val low: Double,
		val open: Double,
		val startTime: String,
		val volume: Double,
)