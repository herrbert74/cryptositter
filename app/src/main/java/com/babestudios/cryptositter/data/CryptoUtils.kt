package com.babestudios.cryptositter.data

import android.util.Base64
import android.util.Log
import java.io.UnsupportedEncodingException
import java.math.BigInteger
import java.nio.charset.StandardCharsets
import java.security.InvalidKeyException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

const val ALGORITHM_HMAC_SHA256 = "HmacSHA256"
const val ALGORITHM_HMAC_SHA384 = "HmacSHA384"
const val ALGORITHM_HMAC_SHA512 = "HmacSHA512"

fun getNonce(): Long {
	return System.currentTimeMillis() - 10000
}

fun encodeBase64(payload: String): String {
	return Base64.encodeToString(payload.toByteArray(), Base64.NO_WRAP)
}

fun hmacDigest(msg: String, keyString: String?, algo: String): String {
	var digest = ""
	try {
		val key = SecretKeySpec(keyString?.toByteArray(charset("UTF-8")), algo)
		val mac = Mac.getInstance(algo)
		mac.init(key)

		val bytes = mac.doFinal(msg.toByteArray(charset("UTF-8")))

		val hash = StringBuffer()
		for (i in bytes.indices) {
			val hex = Integer.toHexString(0xFF and bytes[i].toInt())
			if (hex.length == 1) {
				hash.append('0')
			}
			hash.append(hex)
		}
		digest = hash.toString()
	} catch (e: UnsupportedEncodingException) {
		Log.e("test", "Exception: " + e.message)
	} catch (e: InvalidKeyException) {
		Log.e("test", "Exception: " + e.message)
	} catch (e: NoSuchAlgorithmException) {
		Log.e("test", "Exception: " + e.message)
	}

	return digest
}

fun generateHashSha512(input: String): String {
	return try {
		val md = MessageDigest.getInstance("SHA-512")
		val messageDigest = md.digest(input.toByteArray())
		val no = BigInteger(1, messageDigest)
		var hashtext = no.toString(16)
		while (hashtext.length < 32) {
			hashtext = "0$hashtext"
		}
		hashtext
	} catch (e: NoSuchAlgorithmException) {
		throw RuntimeException(e)
	}
}
