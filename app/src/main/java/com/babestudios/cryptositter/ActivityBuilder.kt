package com.babestudios.cryptositter

import com.babestudios.cryptositter.balances.BalancesActivity
import com.babestudios.cryptositter.balances.BalancesActivityModule
import com.babestudios.cryptositter.charts.ChartActivity
import com.babestudios.cryptositter.charts.ChartActivityModule
import com.babestudios.cryptositter.settings.SettingsActivity
import com.babestudios.cryptositter.settings.SettingsActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

	@ContributesAndroidInjector(modules = [(BalancesActivityModule::class)])
	abstract fun bindBalancesActivity(): BalancesActivity

	@ContributesAndroidInjector(modules = [(ChartActivityModule::class)])
	abstract fun bindChartActivity(): ChartActivity

	@ContributesAndroidInjector(modules = [(SettingsActivityModule::class)])
	abstract fun bindSettingsActivity(): SettingsActivity
}