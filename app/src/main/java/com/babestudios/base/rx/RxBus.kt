package com.babestudios.base.rx


import android.util.SparseArray
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject
import java.util.*


/**
 * This is a slightly modified version of RxBus from this article:
 * https://piercezaifman.com/how-to-make-an-event-bus-with-rxjava-and-rxandroid/
 */
object RxBus {
	private val subjectMap = SparseArray<PublishSubject<Any>>()
	private val subscriptionsMap = HashMap<Any, CompositeDisposable>()

	const val SUBJECT_BALANCES_EXCHANGE_CLICKED = 0
	const val SUBJECT_BALANCES_BALANCE_CLICKED = 1

	/**
	 * IntDef doesn't work in Kotlin, so I switched to ints!
	 */

	//@Retention(SOURCE)
	//@IntDef(SUBJECT_BALANCES_EXCHANGE_CLICKED, SUBJECT_BALANCES_BALANCE_CLICKED)
	internal enum class Subject {
		SUBJECT_BALANCES_EXCHANGE_CLICKED, SUBJECT_BALANCES_BALANCE_CLICKED
	}

	/**
	 * Get the subject or create it if it's not already in memory.
	 */
	private fun getSubject(/*@Subject*/ subjectCode: Int): PublishSubject<Any> {
		var subject: PublishSubject<Any>? = subjectMap.get(subjectCode)
		if (subject == null) {
			subject = PublishSubject.create()
			subject.subscribeOn(AndroidSchedulers.mainThread())
			subjectMap.put(subjectCode, subject)
		}

		return subject
	}

	/**
	 * Get the CompositeSubscription or create it if it's not already in memory.
	 */
	private fun getCompositeSubscription(`object`: Any): CompositeDisposable {
		var compositeDisposable: CompositeDisposable? = subscriptionsMap[`object`]
		if (compositeDisposable == null) {
			compositeDisposable = CompositeDisposable()
			subscriptionsMap[`object`] = compositeDisposable
		}

		return compositeDisposable
	}

	/**
	 * Subscribe to the specified subject and listen for updates on that subject. Pass in an object to associate
	 * your registration with, so that you can unsubscribe later.
	 * <br></br><br></br>
	 * **Note:** Make sure to call [RxBus.unregister] to avoid memory leaks.
	 *
	 *
	 */
	fun subscribe(/*@Subject*/ subject: Int, lifecycle: Any, action: Consumer<Any>) {
		val subscription = getSubject(subject).subscribe(action)
		getCompositeSubscription(lifecycle).add(subscription)
	}

	/**
	 * Unregisters this object from the bus, removing all subscriptions.
	 * This should be called when the object is going to go out of memory.
	 */
	fun unregister(lifecycle: Any) {
		//We have to remove the composition from the map, because once you unsubscribe it can't be used anymore
		val compositeDisposable = subscriptionsMap.remove(lifecycle)
		compositeDisposable?.dispose()
	}

	/**
	 * Publish an object to the specified subject for all subscribers of that subject.
	 */
	fun publish(subject: Int, message: Any?) {
		if (message != null) {
			getSubject(subject).onNext(message)
		}
	}
}// hidden constructor