package com.babestudios.base.views.expandablerecyclerview.viewholders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.babestudios.base.views.expandablerecyclerview.models.ExpandableGroup;

/**
 * ViewHolder for {@link ExpandableGroup#items}
 */
public class ChildViewHolder extends RecyclerView.ViewHolder {

	public ChildViewHolder(View itemView) {
		super(itemView);
	}
}
