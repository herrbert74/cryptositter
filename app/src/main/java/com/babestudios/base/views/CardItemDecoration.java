package com.babestudios.base.views;


import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.babestudios.cryptositter.CryptoSitterApplication;
import com.babestudios.cryptositter.R;

public class CardItemDecoration extends RecyclerView.ItemDecoration {

	private int space = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX,
			CryptoSitterApplication.Companion.getContext().getResources().getDimension(R.dimen.view_margin_normal),
			CryptoSitterApplication.Companion.getContext().getResources().getDisplayMetrics());

	public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
		// Add top margin only for the first item to avoid double space between items
		if (parent.getChildAdapterPosition(view) == 0) {
			outRect.top = space;
		}
		outRect.bottom = space;
	}
}
