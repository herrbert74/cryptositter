package com.babestudios.base.mvp

interface BaseView {
	fun onUnknownError(errorMessage: String)

	fun onTimeout()

	fun onNetworkError()

	fun showProgress()

	fun hideProgress()
}
