package com.babestudios.base.mvp

import androidx.recyclerview.widget.RecyclerView

import java.util.HashMap

abstract class MvpRecyclerAdapter<M, P : BasePresenter<Any, Any>, VH : MvpViewHolder<P>> : RecyclerView.Adapter<VH>() {
	protected val presenters: HashMap<Any, P> = HashMap()

	private fun getPresenter(model: M): P? {
		//System.err.println("Getting presenter for item " + getModelId(model));
		return presenters[getModelId(model)]
	}

	protected abstract fun createPresenter(model: M): P

	protected abstract fun getModelId(model: M): Any

	override fun onViewRecycled(holder: VH) {
		super.onViewRecycled(holder)

		holder.unbindPresenter()
	}

	override fun onFailedToRecycleView(holder: VH): Boolean {
		// Sometimes, if animations are running on the itemView's children, the RecyclerView won't
		// be able to recycle the view. We should still unbind the presenter.
		holder.unbindPresenter()

		return super.onFailedToRecycleView(holder)
	}

	override fun onBindViewHolder(holder: VH, position: Int) {
		holder.bindPresenter(getPresenter(getItem(position)))
	}

	protected abstract fun getItem(position: Int): M
}
