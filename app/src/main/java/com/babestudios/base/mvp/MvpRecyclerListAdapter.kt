package com.babestudios.base.mvp

import java.util.ArrayList

abstract class MvpRecyclerListAdapter<M, P : BasePresenter<Any, Any>, VH : MvpViewHolder<P>> : MvpRecyclerAdapter<M, P, VH>() {
	protected val models: MutableList<M>

	init {
		models = ArrayList()
	}

	fun clearAndAddAll(data: Collection<M>) {
		models.clear()
		presenters.clear()

		for (item in data) {
			addInternal(item)
		}

		notifyDataSetChanged()
	}

	fun addAll(data: Collection<M>) {
		for (item in data) {
			addInternal(item)
		}

		val addedSize = data.size
		val oldSize = models.size - addedSize
		notifyItemRangeInserted(oldSize, addedSize)
	}

	fun addItem(item: M) {
		addInternal(item)
		notifyItemInserted(models.size)
	}

	fun updateItem(item: M) {
		val modelId = getModelId(item)

		// Swap the model
		val position = getItemPosition(item)
		if (position >= 0) {
			models.removeAt(position)
			models.add(position, item)
		}

		// Swap the presenter
		val existingPresenter = presenters[modelId]
		if (existingPresenter != null) {
			existingPresenter.model = item
		}

		if (position >= 0) {
			notifyItemChanged(position)
		}
	}

	fun removeItem(item: M) {
		val position = getItemPosition(item)
		if (position >= 0) {
			models.remove(item)
		}
		presenters.remove(getModelId(item))

		if (position >= 0) {
			notifyItemRemoved(position)
		}
	}

	private fun getItemPosition(item: M): Int {
		val modelId = getModelId(item)

		var position = -1
		for (i in models.indices) {
			val model = models[i]
			if (getModelId(model) == modelId) {
				position = i
				break
			}
		}
		return position
	}

	private fun addInternal(item: M) {
		//System.err.println("Adding item " + getModelId(item));
		models.add(item)
		presenters.put(getModelId(item), createPresenter(item))
	}

	override fun getItemCount(): Int {
		return models.size
	}

	override fun getItem(position: Int): M {
		return models[position]
	}
}
