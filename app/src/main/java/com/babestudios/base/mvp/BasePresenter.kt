package com.babestudios.base.mvp

import java.lang.ref.WeakReference

abstract class BasePresenter<M, V> {

	var view: WeakReference<V>? = null

	var model: M? = null
		set(value) {
			resetState()
			field = value
			if (setupDone()) {
				updateView()
			}
		}

	protected fun resetState() {}

	open fun bindView(view: V) {
		this.view = WeakReference(view)
		if (setupDone()) {
			updateView()
		}
	}

	open fun unbindView() {
		this.view = null
	}

	protected fun view(): V? {
		return if (view == null) {
			null
		} else {
			view!!.get()
		}
	}

	protected abstract fun updateView()

	protected fun setupDone(): Boolean {
		return view() != null && model != null
	}
}
