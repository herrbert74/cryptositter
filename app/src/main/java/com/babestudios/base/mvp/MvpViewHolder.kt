package com.babestudios.base.mvp

import androidx.recyclerview.widget.RecyclerView
import android.view.View

abstract class MvpViewHolder<P : BasePresenter<Any, Any>>(itemView: View) : RecyclerView.ViewHolder(itemView) {
	protected var presenter: P? = null

	fun bindPresenter(presenter: P?) {
		this.presenter = presenter
		presenter?.bindView(this)
	}

	internal fun unbindPresenter() {
		presenter = null
	}

}
