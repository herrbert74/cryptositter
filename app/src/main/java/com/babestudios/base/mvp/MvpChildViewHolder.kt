package com.babestudios.base.mvp

import android.view.View
import com.babestudios.base.views.expandablerecyclerview.viewholders.ChildViewHolder

/**
 * A ChildViewHolder with MvpViewHolder fields and methods mixed in.
 */

class MvpChildViewHolder<P : BasePresenter<*, Any>>
/**
 * Default constructor.
 *
 * @param itemView The [View] being hosted in this ViewHolder
 */
(itemView: View) : ChildViewHolder(itemView) {

	protected var presenter: P? = null

	fun bindPresenter(presenter: P) {
		this.presenter = presenter
		presenter.bindView(this)
	}

	fun unbindPresenter() {
		presenter = null
	}
}
