package com.babestudios.base.mvp

import com.babestudios.base.rx.RxBus
import io.reactivex.disposables.CompositeDisposable

abstract class BaseRxPresenter<M, V> : BasePresenter<M, V>() {

	protected val disposables = CompositeDisposable()

	override fun unbindView() {
		disposables.clear()
		RxBus.unregister(this)
		super.unbindView()
	}
}
