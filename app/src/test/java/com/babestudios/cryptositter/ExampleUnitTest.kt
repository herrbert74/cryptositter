package com.babestudios.cryptositter

import org.junit.Test

import org.junit.Assert.*
import io.reactivex.schedulers.TestScheduler
import io.reactivex.Observable
import java.util.concurrent.TimeUnit


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
	@Test
	fun addition_isCorrect() {
		assertEquals(4, 2 + 2)
	}

	@Test
	@Throws(Exception::class)
	fun flatMapAndConcatMapCompare() {
		val items = arrayListOf("a", "b", "c", "d", "e", "f")


		val scheduler1 = TestScheduler()
		val scheduler2 = TestScheduler()

		Observable.fromIterable(items)
				.flatMap { s ->
					Observable.just(s + "x")
							.delay(5, TimeUnit.SECONDS, scheduler1)
							.doOnNext { print(scheduler1.now(TimeUnit.MILLISECONDS).toString() + " ") }
				}
				.toList()
				.doOnSuccess { println("\nEND:" + scheduler1.now(TimeUnit.MILLISECONDS)) }
				.subscribe()

		scheduler1.advanceTimeBy(1, TimeUnit.MINUTES)

		Observable.fromIterable(items)
				.concatMap { s ->
					Observable.just(s + "x")
							.delay(5, TimeUnit.SECONDS, scheduler2)
							.doOnNext { print(scheduler2.now(TimeUnit.MILLISECONDS).toString() + " ") }
				}
				.toList()
				.doOnSuccess { println("\nEND:" + scheduler2.now(TimeUnit.MILLISECONDS)) }
				.subscribe()

		scheduler2.advanceTimeBy(1, TimeUnit.MINUTES)

	}
}
